<?php

class Kviz {

	protected $databaze;

	public function __construct() {
		$this->databaze = new Databaze();
	}

	public function VytvorNovyKviz($idUzivatele, $nazevKvizu, $public, $otazky, $odpovedi, $typKvizu) {
		$this->databaze->VytvorNovyKviz($idUzivatele, $nazevKvizu, $public, $otazky, $odpovedi, $typKvizu);
	}
	
	public function ExistujiVytvoreneKvizy($idUzivatele) {
		$this->databaze = new Databaze();
		if ($this->databaze->ExistujiVytvoreneKvizy($idUzivatele))
			return true;
		else 
			return false;
	}
	
	public function VratSeznamKvizu($idUzivatele) {
		return $this->databaze->VratSeznamKvizu($idUzivatele);
	}
	
	public function SmazKviz($idUzivatele, $idKvizu) {
		if ($this->databaze->SmazKviz($idKvizu)) 
			return true;
		else return false;
	}
	
	public function ObsahujeKvizMoznosti($idKvizu) {
		return $this->databaze->ObsahujeKvizMoznosti($idKvizu);
	}
	
	public function ExistujeKvizProUzivatele($idUzivatele, $idKvizu) {
		return $this->databaze->ExistujeKvizProUzivatele($idUzivatele, $idKvizu);	
	} 
	
	public function VratVlastnostiKvizu($idKvizu){
		return $this->databaze->VratVlastnostiKvizu($idKvizu);
		
	} 
	
	public function VratOtazkyKvizu($idKvizu) {
		return $this->databaze->VratOtazkyKvizu($idKvizu);
	}
	
	public function VratOdpovediKvizu($idKvizu) {
		return $this->databaze->VratOdpovediKvizu($idKvizu);
	}

	public function UpravKviz($idUzivatele, $idKvizu, $nazevKvizu, $public, $otazky, $odpovedi) {
	
		return $this->databaze->UpravKviz($idUzivatele, $idKvizu, $nazevKvizu, $public, $otazky, $odpovedi);		
	}
	
	public function JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu) {
		return $this->databaze->JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu);
	}

	public function VratDataDoOknaKvizu($idUzivatele, $idKvizu, $cisloOtazky) {

		$vracenaData = $this->databaze->VratDataDoOknaKvizu($idUzivatele, $idKvizu, $cisloOtazky);
		/*$vracenaData["otazka"] = "Jak se da��";
			$vracenaData["odpoved1"] = "dob�e";
			$vracenaData["odpoved2"] = "�patn�";
			$vracenaData["odpoved3"] = "velmi �patn�";*/
		return $vracenaData;
		//echo json_encode($vracenaData);
	}	
}
