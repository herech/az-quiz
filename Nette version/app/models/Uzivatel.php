<?php 

use Nette\Database\Connection;
use Nette\Mail\Message;

class Uzivatel { 
	
	protected $databaze;
	
	public function __construct() {
		$this->databaze = new Databaze();
	}
	
	public function RegistrovatSe($login, $password) {
		if ($this->databaze->OverUnikatnostUzivatele($login)) {
			$this->databaze->RegistrovatUzivatele($login, $password, $login);
			return true;
		}
		else {
			return false;
		}
	}
	
	public function NastavNoveHeslo($email, $heslo, $token, $adresa) {
		
		if ($this->databaze->OverExistenciEmailu($email)) {			
				$this->databaze->NastavToken($email, $token);
				$mail = new Message;
				$mail->setFrom("az-kviz@set-new-password.cz")
				->addTo($email)
				->setSubject("Potvrzení změny hesla")
				->setBody("Dobrý den," . PHP_EOL . "Požádali jste o nastavení nového hesla pro váš účet v online hře az kvíz. Jako nové heslo jste si zvolili: " . $heslo . ". K úspěšnému potvrzení změny hesla prosím následujte následující odkaz " . $adresa . "." . PHP_EOL . "Pokud jste nepožádali o změnu hesla, či dokonce ani nemáte vytvořen účet v této online hře, tak pouze někdo zneužil vaši emailovou adresu a prosím ignorujte tento mail.")
				->send();
				return true;
		}
		else {
			return false;
		}
	}
	
	public function PotvrdNastaveniNovehoHesla($email, $token, $password) {
		if ($this->databaze->OverPlatnostUdaju($email, $token)) {			
			
			$this->databaze->NastavNoveHeslo($password, $email);			
			return true;
		}
		else return false;
	}
	
	public function VratUzivatelskeUdaje($id) {
		return $this->databaze->VratUzivatelskeUdaje($id);
	}
	
	public function ZmenHeslo($id, $passwordOld, $passwordNew) {
			if ($this->databaze->ZmenHeslo($id, $passwordOld, $passwordNew))
				return true;
			else return false;
	}
	
	public function ZmenEmail($id, $passwordCurrent, $emailNew) {

			if ($this->databaze->ZmenEmail($id, $passwordCurrent, $emailNew))
				return true;
			else return false;
	}

}