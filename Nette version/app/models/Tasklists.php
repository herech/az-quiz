<?php

use Nette\Database\Connection,
    Nette\Database\Table\Selection;


/**
 * Třída, která reprezentuje tabulku se seznamy úkolů.
 */
class Tasklists extends Selection
{
	public function __construct(Connection $connection)
	{
		parent::__construct('tasklist', $connection);
	}
}