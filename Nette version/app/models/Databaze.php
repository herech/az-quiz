<?php 

use Nette\Utils\Validators;

class Databaze { 

	protected $databaze;
	protected $spojeni = array();

	public function __construct() {
			try {
				$this->databaze = new Nette\Database\Connection('mysql:host=localhost;dbname=azkviz', 'root', '', NULL);
			}
			catch(PDOException $e)
			{
				exit("Bohůžel se nepodařilo vytvořit databázové spojení.");
			}
	}
	
	/*------------------------------METODY V PROCESU REGISTRACE-------------------------------------*/	

	public function OverUnikatnostUzivatele($login) {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_users WHERE login = ?", $login);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 0)
			return true;
		else return false;
		
	}
	public function RegistrovatUzivatele($login, $password, $email) {
		
		$password = md5($password);
		$dotaz = $this->databaze->exec("INSERT INTO az_users (login, password, email, date) VALUES (?, ?, ?, ?)", $login,$password,$email,new DateTime);
		if ($dotaz != 1)
			throw new DatabaseException("Nepodařilo se zaregistrovat");

	}
/*------------------------------METODY V PROCESU PŘIHLÁŠENÍ-------------------------------------*/		

	public function OverPrihlasovaciUdajeUzivatele($login, $password) {
		
	}
	
	/*public function VratUserId() {
		return $this->userId;
	}*/
	
	/*------------------------------METODY V PROCESU NASTAVENÍ NOVÉHO HESLA-------------------------------------*/		

	public function OverExistenciEmailu($email) {
		
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_users WHERE email = ?", $email);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;

	}
	
	public function NastavToken($email, $token) {
	
	$this->databaze->exec("UPDATE az_users SET password_token = ?,password_token_generated = ? WHERE email = ?",$token,new DateTime,$email);

	}
	
	public function OverPlatnostUdaju($email, $token) {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_users WHERE email = ? AND password_token = ? AND password_token_generated >= ? - INTERVAL 1 DAY", $email, $token, new DateTime);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;
		
	}
	
	public function NastavNoveHeslo($password, $email) {
		
		$this->databaze->exec("UPDATE az_users SET password_token_generated = NULL, password_token = NULL, password = ? WHERE email = ?", $password, $email);
		
	}
	
	/*------------------------------METODY V PROCESU SPRÁVY ÚČTU-------------------------------------*/		
	
	public function VratUzivatelskeUdaje($id) {
		
		$dotaz = $this->databaze->query("SELECT id, login, email, date FROM az_users WHERE id = ?", $id);
		
		$uzivatel = new stdClass;
		
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$uzivatel->id = $radek->id;
			$uzivatel->login = $radek->login;
			$uzivatel->email = $radek->email;
			$uzivatel->date = $radek->date;
		}
		return $uzivatel;
	}
	
	public function ZmenEmail($id, $soucasneHeslo, $novyEmail) {
		$dotazAliasPocetOvlivnenychSloupcu = $this->databaze->exec("UPDATE az_users SET email = ? WHERE id = ? AND password = ?",$novyEmail,$id, md5($soucasneHeslo));
		if ($dotazAliasPocetOvlivnenychSloupcu == 1)	
			return true;
		else return false;
	}
	
	public function ZmenHeslo($id, $stareHeslo, $noveHeslo) {
		$dotazAliasPocetOvlivnenychSloupcu = $this->databaze->exec("UPDATE az_users SET password = ? WHERE id = ? AND password = ?", md5($noveHeslo), $id, md5($stareHeslo));
		if ($dotazAliasPocetOvlivnenychSloupcu == 1)	
			return true;
		else return false;
	}
	
/*------------------------------METODY V PROCESU VYTVOŘENÍ NOVÉHO KVÍZU-------------------------------------*/	

	public function VytvorNovyKviz($idUzivatele, $nazevKvizu, $public, $otazky, $odpovedi, $typKvizu) {
		
		if ($public)
			$public = "ano";
		else
			$public = "ne";
		
		$pocetOtazek = count($otazky);	
		if ($typKvizu == "kvizBezMoznosti") {
		
			$this->databaze->exec("INSERT INTO az_quizs (id_az_user, name, public, date, have_options) VALUES (?, ?, ?, ?, 'ne')", $idUzivatele, $nazevKvizu, $public, new DateTime);

			$idKvizu = $this->databaze->lastInsertId();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				$this->databaze->exec("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES (?, ?, ?, ?)", $idKvizu,$idUzivatele,$aktualniOtazka, ($i+1));

				$idOtazky = $this->databaze->lastInsertId();

					$aktualniOdpoved = $odpovedi[$i];
					
					$this->databaze->exec("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES (?, ?, ?, ?, 'ano', '1')", $idKvizu, $idOtazky, $idUzivatele, $aktualniOdpoved);
			}
		
		}
		else {
			
			$this->databaze->exec("INSERT INTO az_quizs (id_az_user, name, public, date) VALUES (?, ?, ?, ?)", $idUzivatele, $nazevKvizu, $public, new DateTime);

			$idKvizu = $this->databaze->lastInsertId();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				
				$this->databaze->exec("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES (?, ?, ?, ?)", $idKvizu,$idUzivatele,$aktualniOtazka, ($i+1));
				
				$idOtazky = $this->databaze->lastInsertId();
				
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					
					$this->databaze->exec("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES (?, ?, ?, ?, ?, ?)", $idKvizu, $idOtazky, $idUzivatele, $aktualniOdpoved, $spravnaOdpoved, $poradiOdpovedi);
						
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
	}
/*------------------------------METODY V PROCESU ZOBRAZENÍ VLASTNÍCH KVÍZŮ-------------------------------------*/		
	
	public function ExistujiVytvoreneKvizy($idUzivatele) {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE id_az_user = ?",$idUzivatele);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] >= 1)
			return true;
		else return false;
	}
	
	public function VratSeznamKvizu($idUzivatele) {
		$seznamKvizu = array();
		$i = 0;
		$dotaz = $this->databaze->query("SELECT id, name, date, have_options, public FROM az_quizs WHERE id_az_user = ? ORDER BY date DESC",$idUzivatele);

		while($radek = $dotaz->fetch(PDO::FETCH_ASSOC)) {
			$seznamKvizu[$i]["id"] = $radek["id"];
			$seznamKvizu[$i]["name"] = $radek["name"];
			$seznamKvizu[$i]["date"] = $radek["date"];
			$seznamKvizu[$i]["have_options"] = $radek["have_options"];
			$seznamKvizu[$i]["public"] = $radek["public"];
			$i += 1;
			//array (array("id" => , "name" =>, "date" => ), array("id" => , "name" =>, "date" => ))
		}		
		return $seznamKvizu;
	}
	
/*------------------------------METODY V PROCESU ÚPRAVY KVIZU-------------------------------------*/		
	
	public function SmazKviz($idUzivatele, $idKvizu){
		$pocetSmazanychZaznamu = $this->databaze->exec("DELETE FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);

		if ($pocetSmazanychZaznamu == 1) {
			$this->databaze->exec("DELETE FROM az_questions WHERE id_az_quiz = ?", $idKvizu);
			$this->databaze->exec("DELETE FROM az_answers WHERE id_az_quiz = ?", $idKvizu);
			return true;
		}
		else 
			return false;
	}
	
	public function UpravKviz($idUzivatele, $idKvizu, $nazevKvizu, $public, $otazky, $odpovedi) {

		$pocetOtazek = count($otazky);	
		
		$pocetOdpovedi = count($odpovedi);

		$dotaz = $this->databaze->exec("UPDATE az_quizs SET name = ?, public = ? WHERE id = ? AND id_az_user = ?", $nazevKvizu, $public, $idKvizu, $idUzivatele);
		
		if (!$this->ObsahujeKvizMoznosti($idKvizu)) {
		
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				
				$dotaz = $this->databaze->exec("UPDATE az_questions SET name = ? WHERE id_az_quiz = ? AND order_number = ?", $aktualniOtazka, $idKvizu, ($i+1));
				
				$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, ($i+1));
				$objektVysledku = $dotaz->fetch();			
				$idOtazky = $objektVysledku[0];
					
				$aktualniOdpoved = $odpovedi[$i];
				
				$this->databaze->exec("UPDATE az_answers SET name = ? WHERE id_az_quiz = ? AND id_az_question = ? AND order_number = '1'", $aktualniOdpoved, $idKvizu, $idOtazky);
						
			}
		}
		else {
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				$this->databaze->exec("UPDATE az_questions SET name = ? WHERE id_az_quiz = ? AND order_number = ?",$aktualniOtazka, $idKvizu, ($i+1));

				$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, ($i+1));
				$objektVysledku = $dotaz->fetch();			
				$idOtazky = $objektVysledku[0];
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					
					$this->databaze->exec("UPDATE az_answers SET name = ?, right_answer = ? WHERE id_az_quiz = ? AND id_az_question = ? AND order_number = ?",$aktualniOdpoved,$spravnaOdpoved,$idKvizu,$idOtazky,$poradiOdpovedi);
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
	}
	
	public function ExistujeKvizProUzivatele($idUzivatele, $idKvizu) {
	
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;
	
	} 
	
	public function VratVlastnostiKvizu($idKvizu){
		$dotaz = $this->databaze->query("SELECT name, public FROM az_quizs WHERE id = ?", $idKvizu);
		
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$vlastnostiKvizu["name"] = $radek->name;
			$vlastnostiKvizu["public"] = $radek->public;
		}	
		
		return $this->OdescapujData($vlastnostiKvizu);
		
	} 
	public function VratOtazkyKvizu($idKvizu) {
		$dotaz = $this->databaze->query("SELECT name FROM az_questions WHERE id_az_quiz = ? ORDER BY order_number ASC", $idKvizu);
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$otazkyKvizu[] = $radek->name;
		}	
		
		return $this->OdescapujData($otazkyKvizu);
	}
	
	public function VratOdpovediKvizu($idKvizu) {
		$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_quiz = ? ORDER BY id ASC", $idKvizu);
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$odpovediKvizu[] = $radek->name;
		}	
		
		return $this->OdescapujData($odpovediKvizu);
	}
	
	public function ObsahujeKvizMoznosti($idKvizu) {
	
		$dotaz = $this->databaze->query("SELECT have_options FROM az_quizs WHERE id = ?", $idKvizu);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == "ano") {
			return true;
		}
		else return false;
	}
/*------------------------------METODY V PROCESU Vyhledávání-------------------------------------*/		
	public function VratPocetVysledkuProDotaz($dotaz) {
		if ($dotaz == "*") { 
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE public = 'ano'");
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs");
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		else {
			$dotaz = '%'.$dotaz.'%';
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE public = 'ano' AND name LIKE ?",$dotaz);
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		return $pocetVysledku;
	}
	
	public function VratSeznamKvizuProDotaz($dotaz) {
		$seznamKvizu = array();
		$i = 0;
		if ($dotaz == "*") {
			$query = $this->databaze->query("SELECT * FROM az_quizs WHERE public = 'ano'");
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$query = $this->databaze->query("SELECT * FROM az_quizs");
		}
		else {
			$dotaz = '%'.$dotaz.'%';
			$query = $this->databaze->query("SELECT * FROM az_quizs WHERE public = 'ano' AND name LIKE ?", $dotaz);
		}
		while ($radek = $query->fetch(PDO::FETCH_OBJ)) {
			$seznamKvizu[$i]["id"] = $radek->id;
			$seznamKvizu[$i]["name"] = $radek->name;
			$dotaz1 = $this->databaze->query("SELECT login FROM az_users WHERE id = ?", $radek->id_az_user);
			$objektVysledku = $dotaz1->fetch();
			$login = $objektVysledku[0];
			$seznamKvizu[$i]["login"] = $login;
			$i += 1;
		}
		return $seznamKvizu;
	}
/*------------------------------METODY V PROCESU SAMOTNÉ HRY AZ KVÍZ-------------------------------------*/		
	
	public function JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu) {
		$dotaz = $this->databaze->query("SELECT * FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);
		if ($dotaz->columnCount() >= 1) 
			return true;
		else {
		$dotaz = $this->databaze->query("SELECT * FROM az_quizs WHERE id = ? AND public = 'ano'", $idKvizu);
		if ($dotaz->columnCount() >= 1) 
			return true;
		else 
			return false;
		}
	}
	
	public function VratDataDoOknaKvizu($idUzivatele, $idKvizu, $cisloOtazky) {
		if ($this->JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu)) {
			$dotaz = $this->databaze->query("SELECT name FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, $cisloOtazky);
			$objektVysledku = $dotaz->fetch();
			$vracenaData["otazka"] = $objektVysledku[0];
			
			$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, $cisloOtazky);
			$objektVysledku = $dotaz->fetch();
			$idOtazky = $objektVysledku[0];
			
			if ($this->ObsahujeKvizMoznosti($idKvizu)) {
			
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '1'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved1"] = $objektVysledku[0];
				
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '2'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved2"] = $objektVysledku[0];
				
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '3'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved3"] = $objektVysledku[0];
			}
			else {
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '1'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved1"] = $objektVysledku[0];
			}
			
			$vracenaData = $this->OdescapujData($vracenaData);
			
			return $vracenaData;
		}
		else return false;
	}
	
	public function OdescapujData($data) {
		$odescapovanaData = array();
		if (is_array($data)) {
		 foreach ($data as $index => $prvek) {
			$odescapovanaData[$index] = preg_replace("/\\\\+'/","'",$prvek);
		 }
		}
		else {
			$odescapovanaData = preg_replace("/\\\\+'/","'",$data);
		}
		return $odescapovanaData;
	}
}