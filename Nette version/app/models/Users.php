<?php

use Nette\Database\Connection,
    Nette\Database\Table\Selection;


/**
 * Třída, která reprezentuje tabulku uživatelů.
 */
class Users extends Selection
{
	public function __construct(Connection $connection)
	{
		parent::__construct('az_users', $connection);
	}
}