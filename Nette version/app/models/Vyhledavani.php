<?php
class Vyhledavani {	
	
	protected $dotaz;
	protected $databaze;
	
	public function __construct($dotaz) {
		$this->dotaz = rawurldecode($dotaz);
		$this->databaze = new Databaze();
	} 
	
	public function VratPocetVysledkuProDotaz() {
		return $this->databaze->VratPocetVysledkuProDotaz($this->dotaz);
	}

	public function VratSeznamKvizuProDotaz() {
		$seznamKvizu = $this->databaze->VratSeznamKvizuProDotaz($this->dotaz);
		return $seznamKvizu;
	}
	
}
?>