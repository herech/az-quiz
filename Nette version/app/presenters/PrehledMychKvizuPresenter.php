<?php
use Nette\Application\UI\Form;
use Nette\Utils\Validators;
class PrehledMychKvizuPresenter extends BasePresenter {

	protected $idKvizu;
	protected $isInt = false;

	public function actionUpravKviz($idKvizu) {
		$this->setView('upraveniKvizu');
		$this->idKvizu = $idKvizu;
		if (Validators::isNumericInt($idKvizu))
			$this->isInt = true;
	}
	
	public function actionSmazKviz($idKvizu) {
		if ($this->getUser()->isLoggedIn()) {
			$kviz = new Kviz();
			if ((Validators::isNumericInt($idKvizu)) and ($kviz->SmazKviz($this->getUser()->id,$idKvizu)))
				$this->flashMessage("Kvíz byl úspěšně smazán.","success");
			else
				$this->flashMessage("Kvíz nemohl být smazán, protože neexistuje.","error");
			$this->redirect("PrehledMychKvizu:");
		}	
	}

	public function renderDefault() {
		$kviz = new Kviz();
		$this->template->existujiVytvoreneKvizy = $kviz->ExistujiVytvoreneKvizy($this->getUser()->id);
		$seznamKvizu = $kviz->VratSeznamKvizu($this->getUser()->id);
		$this->template->pocetKvizu = count($seznamKvizu);
		$this->template->kvizy = $seznamKvizu;
	}
	
	public function renderUpraveniKvizu() {
		$kviz = new Kviz();
		if (($this->isInt == false) or ($kviz->ExistujeKvizProUzivatele($this->getUser()->id, $this->idKvizu) == false))
			$this->template->existujeKvizProUzivatele = false;
		else {
			$this->template->existujeKvizProUzivatele = true; 
			$this->template->idKvizu = $this->idKvizu;
			$this->template->vlastnostiKvizu = $kviz->VratVlastnostiKvizu($this->idKvizu);
			$this->template->otazky = $kviz->VratOtazkyKvizu($this->idKvizu);
			$this->template->odpovedi = $kviz->VratOdpovediKvizu($this->idKvizu);
			$this->template->obsahujeKvizMoznosti = $kviz->ObsahujeKvizMoznosti($this->idKvizu);
		}
	}
	
	protected function createComponentUpraveniKvizuForm() {
		$kviz = new Kviz();
		$vlastnostiKvizu =  $kviz->VratVlastnostiKvizu($this->idKvizu);
		if ($vlastnostiKvizu["public"] == "ano")
			$public = TRUE;
		else $public = FALSE;
		
		$form = new Form();
		$containerOtazky = $form->addContainer('otazky');
		for ($i = 1; $i <= 28;$i++) {
			$containerOtazky->addText($i)
			->setAttribute("class", "otazky")
			->setRequired("Vyplňte všechny otázky!")
			->setAttribute("autocomplete","on");
		}
		$containerOdpovedi = $form->addContainer('odpovedi');
		for ($i = 1; $i <= 84;$i++) {
			$containerOdpovedi->addText($i);
		}
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText("nazevKvizu","")
			->setAttribute("id","nazev-kvizu")
			->setRequired("Zadejte název kvízu!")
			->setAttribute("style","margin-bottom:15px;margin-left: 20px;width: 94%;");
		$form->addCheckbox("public")
			->setAttribute("id", "verejny-kviz")
			->setAttribute("style","display:inline;")
			->setDefaultValue($public)
			->setAttribute("checked",$public);
		/*$form->addText("otazky[]")
			->setAttribute("class", "otazky")
			->setRequired("Vyplňte všechny otázky!")
			->setAttribute("autocomplete","on");
		$form->addText("odpovedi[]")
			->setRequired("Vyplňte všechny odpovědi!");*/
		$form->addSubmit("submit","Upravit kvíz");
		$form->onSuccess[] = callback($this, "upraveniKvizuFormSubmitted");
		return $form;
	}
	
	public function upraveniKvizuFormSubmitted(Form $form) {
		if ($this->getUser()->isLoggedIn()) {
			
			$kviz = new Kviz();
			$obsahujeKvizMoznosti = $kviz->ObsahujeKvizMoznosti($this->idKvizu);
			
			$error = false;
			$iteracniPromenna = 0;
			if ($obsahujeKvizMoznosti) 
				$maxIndex = 83;		
			else 
				$maxIndex = 27;
			$odpovedi = array();
			foreach ($form->values->odpovedi as $odpoved) {
				if ($odpoved == "")
					$error = true;
				else
					$odpovedi[] = $odpoved;
				$iteracniPromenna += 1;
				if ($iteracniPromenna > $maxIndex)
					break;
			}
			
			$otazky = array();
			foreach ($form->values->otazky as $otazka) {
				if ($otazka == "")
					$error = true;
				else
					$otazky[] = $otazka;
			}
			
			if ($error) {
					$form->addError("Nevyplnili jste všechna políčka! ");
			}
			else {
				$user = $this->getUser();
				/*if(Validators::isList($form->values->otazky)) {
					$this->flashMessage("OK","success");
				}
				else {
					$this->flashMessage("KO","error");
				}
					
				$this->flashMessage($form->values->nazevKvizu . $form->values->public . implode("||", $form->values->otazky) . $form->values->odpovedi . implode("|||", $form->values->typKvizu),"success");
				$this->redirect("this");*/

				$kviz->UpravKviz($user->id, $this->idKvizu, $form->values->nazevKvizu, $form->values->public, $otazky, $odpovedi);
				$this->flashMessage("Kvíz byl úspěšně upraven!","success");

				$this->redirect("this");	
			}
		}
	}

}