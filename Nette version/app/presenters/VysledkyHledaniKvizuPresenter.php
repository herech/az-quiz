<?php

use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;

class VysledkyHledaniKvizuPresenter extends BasePresenter {
	
	protected $dotaz;
	protected $bylyPredanyParametry;
	protected $pocetVysledkuProDotaz;
	protected $seznamKvizu;
	protected $pocetKvizu;
	
	public function actionDefault($dotaz) {
		if ((isset($dotaz)) && ($dotaz != "")) {
				$this->dotaz = $dotaz;
				$this->bylyPredanyParametry = true;
				$vyhledavani = new Vyhledavani($this->dotaz);
				$this->pocetVysledkuProDotaz = $vyhledavani->VratPocetVysledkuProDotaz();
				if ($this->pocetVysledkuProDotaz >= 1) {
					$this->seznamKvizu = $vyhledavani->VratSeznamKvizuProDotaz();
				}
				else {
					$this->seznamKvizu = array();
				}
				$this->pocetKvizu = count($this->seznamKvizu);
			}
			
		else 
			$this->bylyPredanyParametry = false;
	}
	
	public function renderDefault() {
		$this->template->bylyPredanyParametry = $this->bylyPredanyParametry;
		if ($this->bylyPredanyParametry) {
			$this->template->dotaz = $this->dotaz;
			$this->template->pocetVysledkuProDotaz = $this->pocetVysledkuProDotaz;
			$this->template->pocetKvizu = $this->pocetKvizu;
			$this->template->seznamKvizu = $this->seznamKvizu;
		}
	}
}