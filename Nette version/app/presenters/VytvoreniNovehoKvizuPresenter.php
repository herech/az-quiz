<?php

use Nette\Application\UI\Form;

use Nette\Utils\Validators;

class VytvoreniNovehoKvizuPresenter extends BasePresenter {

	protected function createComponentVytvoreniKvizuForm() {
		$form = new Form();
		$containerOtazky = $form->addContainer('otazky');
		for ($i = 1; $i <= 28;$i++) {
			$containerOtazky->addText($i)
			->setAttribute("class", "otazky")
			->setRequired("Vyplňte všechny otázky!")
			->setAttribute("autocomplete","on");
		}
		$containerOdpovedi = $form->addContainer('odpovedi');
		for ($i = 1; $i <= 84;$i++) {
			$containerOdpovedi->addText($i);
		}
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText("nazevKvizu","")
			->setAttribute("id","nazev-kvizu")
			->setRequired("Zadejte název kvízu!")
			->setAttribute("style","margin-bottom:15px;margin-left: 20px;width: 94%;");
		$form->addCheckbox("public")
			->setAttribute("id", "verejny-kviz")
			->setAttribute("style","display:inline;")
			->setDefaultValue(TRUE)
			->setAttribute("checked","true");
		/*$form->addText("otazky[]")
			->setAttribute("class", "otazky")
			->setRequired("Vyplňte všechny otázky!")
			->setAttribute("autocomplete","on");
		$form->addText("odpovedi[]")
			->setRequired("Vyplňte všechny odpovědi!");*/
		$form->addHidden("typKvizu");
		$form->addSubmit("submit","Vytvořit kvíz");
		$form->onSuccess[] = callback($this, "vytvoreniKvizuFormSubmitted");
		return $form;
	}
	
	public function vytvoreniKvizuFormSubmitted(Form $form) {
		if ($this->getUser()->isLoggedIn()) {
			
			$error = false;
			$iteracniPromenna = 0;
			if ($form->values->typKvizu == "kvizBezMoznosti") 
				$maxIndex = 27;			
			else 
				$maxIndex = 83;
			$odpovedi = array();
			foreach ($form->values->odpovedi as $odpoved) {
				if ($odpoved == "")
					$error = true;
				else
					$odpovedi[] = $odpoved;
				$iteracniPromenna += 1;
				if ($iteracniPromenna > $maxIndex)
					break;
			}
			
			$otazky = array();
			foreach ($form->values->otazky as $otazka) {
				if ($otazka == "")
					$error = true;
				else
					$otazky[] = $otazka;
			}
			
			if ($error) {
				if ($form->values->typKvizu == "kvizBezMoznosti") 
					$form->addError("Nevyplnili jste všechna políčka! Znovu vyberte již vámi zvolený kvíz bez možností a dolňte chybějící políčka.");
				else
					$form->addError("Nevyplnili jste všechna políčka! Znovu vyberte již vámi zvolený kvíz s možnostmi a dolňte chybějící políčka.");
			}
			else {
				$user = $this->getUser();
				/*if(Validators::isList($form->values->otazky)) {
					$this->flashMessage("OK","success");
				}
				else {
					$this->flashMessage("KO","error");
				}
					
				$this->flashMessage($form->values->nazevKvizu . $form->values->public . implode("||", $form->values->otazky) . $form->values->odpovedi . implode("|||", $form->values->typKvizu),"success");
				$this->redirect("this");*/

					$kviz = new Kviz();
					$kviz->VytvorNovyKviz($user->id, $form->values->nazevKvizu, $form->values->public, $otazky, $odpovedi, $form->values->typKvizu);

				$this->flashMessage("Kvíz byl úspěšně vytvořen!","success");
				$this->redirect("this");	
			}
		}
	}
	
	
	public function renderDefault() {

	} 

}