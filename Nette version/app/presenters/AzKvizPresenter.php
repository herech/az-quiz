<?php

use Nette\Utils\Validators;
use Nette\Http\Request;

class AzKvizPresenter extends BasePresenter {
	
	protected $bylyPredanyParametry;
	protected $idKvizu;
	protected $nazevKvizu;
	protected $jeKvizVerejnyProSoucasnehoUzivatele;
	protected $jeZapnutySpecialniMod;
	
	public function actionDefault($idKvizu, $nazevKvizu) {
		if ((isset($idKvizu)) && ($idKvizu != "") && (Validators::isNumericInt($idKvizu)) && (isset($nazevKvizu)) && ($nazevKvizu != "")) {
				$this->bylyPredanyParametry = true;
				$this->idKvizu = $idKvizu;
				$this->nazevKvizu = rawurldecode($nazevKvizu);
			}
			
		else 
			$this->bylyPredanyParametry = false;
	}
	
	public function beforeRender() {
		$kviz = new Kviz();
		$this->jeKvizVerejnyProSoucasnehoUzivatele = $kviz->JeKvizVerejnyProSoucasnehoUzivatele($this->getUser()->id,$this->idKvizu);
		$httpRequest = $this->context->httpRequest;
		$jeZapnutySpecialniMod = $httpRequest->getCookie('jeZapnutySpecialniMod');

		if ($jeZapnutySpecialniMod == "ano")
			$this->jeZapnutySpecialniMod = true;
		else
			$this->jeZapnutySpecialniMod = false;
	}
	
	public function renderDefault() {
		$this->template->bylyPredanyParametry = $this->bylyPredanyParametry;
		if ($this->bylyPredanyParametry) {
			$this->template->idKvizu = $this->idKvizu;
			$this->template->nazevKvizu = $this->nazevKvizu;
			$this->template->jeKvizVerejnyProSoucasnehoUzivatele = $this->jeKvizVerejnyProSoucasnehoUzivatele;
			$this->template->jeZapnutySpecialniMod = $this->jeZapnutySpecialniMod;
		}
	}
}