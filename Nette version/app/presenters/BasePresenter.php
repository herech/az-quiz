<?php

/**
 * Base class for all application presenters.
 *
 * @author     John Doe
 * @package    MyApplication
 */
use Nette\Application\UI\Form; 
use Nette\Utils\Html;
use Nette\Database\Connection;
use Nette\Security as NS;
 
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	
/*	public function handleSignOut()
	{
		$this->getUser()->logout();
		$this->redirect('Sign:in');
	}
	
	public function beforeRender() {
		$this->template->taskLists = $this->context->createTasklists()->order('title ASC');
	}
	protected function createComponentNewTasklistForm() {
		if (!$this->getUser()->isLoggedIn()) {
			throw new Nette\Application\ForbiddenRequestException();
		}
		
		$form = new Form;
		$form->addText('title','Název: ', 15, 50)->addRule(Form::FILLED, 'Musíte zadat název seznamu prvků.');
		$form->addSubmit('create', 'Vytvořit');
		$form->onSuccess[] = callback($this, 'newTaskListFormSubmitted');
		return $form;
	}
	
	public function newTaskListFormSubmitted(Form $form) {
		$tasklist = $this->context->createTasklists()->insert(array('title' => $form->values->title));
		$this->flashMessage('Seznam úkolů založen.', 'success');
		$this->redirect('Task:default', $tasklist->id);
	}
	
	public function handleSingOut() {
		$this->getUser->logout();
		$this->redirect('this');
	}*/
	
	protected function createComponentRegistrationForm() {
		$vasEmail = Html::el('strong', 'Váš email');
		$form = new Form();
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText('email', Html::el("")
        ->setHtml('<strong>Váš email</strong> (bude sloužit jako vaše přihlašovací jméno):'))
				->setAttribute('type', 'email')
				->setAttribute('id', 'email1')
				->addRule(Form::EMAIL, 'Zadejte platný email!')
				->setRequired('Zadejte platný email!');
		$form->addPassword('password', Html::el("")->setHtml("<strong>Vaše heslo</strong> (doporučeno min. 10 znaků):"))
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password1')
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired('Zadejte Heslo!');
		$form->addPassword('password1', Html::el("")->setHtml("<strong>Vaše heslo</strong> (pro ověření):"))
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password2')
				->setRequired('Zadejte heslo pro ověření!')
				->addRule(Form::EQUAL, 'Zadaná hesla nejsou stejná!', $form['password']);
		$form->addText('inputPrvniNahodneCislo')
				->setAttribute('class', 'inputPrvniNahodneCislo')
				->setAttribute('disabled', 'true')
				->setAttribute('style', 'display:inline;width:50px');
		$form->addText('inputDruheNahodneCislo')
				->setAttribute('class', 'inputDruheNahodneCislo')
				->setAttribute('disabled', 'true')
				->setAttribute('style', 'display:inline;width:50px');
		$form->addText('inputVysledekSouctu')
				->setAttribute('class', 'inputVysledekSouctu')
				->setAttribute('style', 'display:inline;width:50px')
				->setRequired('Zadejte kontrolní součet!');
		$form->addSubmit('submit', 'Registrovat se');
		$form->onSuccess[] = callback($this, 'registrationFormSubmitted');
		return $form;
	}
	
	public function registrationFormSubmitted(Form $form) {
		if (($form->values->inputPrvniNahodneCislo + $form->values->inputDruheNahodneCislo) != $form->values->inputVysledekSouctu) {
			$form->addError("Kontrolní součet nesouhlasí!");
		}
		else  {
			
			$uzivatel = new Uzivatel();
			
			if ($uzivatel->RegistrovatSe($form->values->email, $form->values->password)) {
				$this->flashMessage("Registrace byla úspěšná", 'success');
			}
			else {
				$this->flashMessage("Registrace se bohůžel nezdařila, login který jste si vybrali je už pravděpodobně obsazený.", 'error');
			}
			$this->redirect('this');
			
		}
	}
	
	protected function createComponentSignInForm() {
		$form = new Form();
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText('email', Html::el("")
        ->setHtml('<strong>Vaše přihlašovací jméno:</strong>'))
				->setAttribute('type', 'email')
				->setAttribute('id', 'email2')
				->addRule(Form::EMAIL, 'Zadejte platný email!')
				->setRequired('Zadejte platný email!');
		$form->addPassword('password', Html::el("")->setHtml("<strong>Vaše heslo:</strong>"))
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password3')
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired('Zadejte Heslo!');
		$form->addSubmit('submit', 'Přihlásit');
		$form->onSuccess[] = callback($this, 'signInFormSubmitted');
		return $form;
	}
	
	public function signInFormSubmitted(Form $form) {
		
		try {
			$user = $this->getUser();
			$user->setExpiration(0, TRUE);
			$user->login($form->values->email, $form->values->password);
			$this->flashMessage("Přihlášení bylo úspěšné", 'success');
			$this->redirect('this');
		}
		catch (NS\AuthenticationException $e) {
			$form->addError('Neplatné uživatelské jméno nebo heslo.');
		}
	}
	
	protected function createComponentNewPasswordForm() {
		$form = new Form();
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText("email", Html::el("")->setHtml("<strong>Váš email:</strong></label>"))
				->addRule(Form::EMAIL, "Zadejte platný email!")
				->setRequired("Zadejte platný email")
				->setAttribute("type", "email")
				->setAttribute("id", "email3");
		$form->addPassword("password", Html::el("")->setHtml("<strong>Vaše nové heslo:</strong>"))
				->setAttribute("autocomplete", "off")
				->setAttribute("id", "password4")
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired("Zadejte heslo!");
		$form->addText('inputPrvniNahodneCislo')
				->setAttribute('class', 'inputPrvniNahodneCislo')
				->setAttribute('style', 'display:inline;width:50px');
		$form->addText('inputDruheNahodneCislo')
				->setAttribute('class', 'inputDruheNahodneCislo')
				->setAttribute('style', 'display:inline;width:50px');
		$form->addText('inputVysledekSouctu')
				->setAttribute('class', 'inputVysledekSouctu')
				->setAttribute('style', 'display:inline;width:50px')
				->setRequired('Zadejte kontrolní součet!');
		$form->addSubmit("submit", "Odeslat");
		$form->onSuccess[] = callback($this, "newPasswordFormSubmitted");
		return $form;
	}
	
	public function newPasswordFormSubmitted(Form $form) {
		if (($form->values->inputPrvniNahodneCislo + $form->values->inputDruheNahodneCislo) != $form->values->inputVysledekSouctu) {
		$form->addError('Nesprávný kontrolní součet.');
		}
		else  {
			$token = md5(uniqId(mt_rand(), true));
			$email = $form->values->email;
			$password = $form->values->password;
			$adresa = $this->link("ConfirmNewPassword:confirm", array("email" => $email, "password_token" => $token, "password" => md5($password)));
			$adresa = $_SERVER['SERVER_NAME'] . $adresa;
			$uzivatel = new Uzivatel();
			if ($uzivatel->NastavNoveHeslo($email, $password, $token, rawurldecode($adresa))) 
				$this->flashMessage("Na zadaný email byl odeslán odkaz pro potvrzení nového hesla!". rawurldecode($adresa) ."", 'success');
			else
				$this->flashMessage("Vaše žádost nebyla zpracována, protože uživatel se zadaným emailem neexistuje!", 'error');
			$this->redirect('this');
		}
	}
	
	protected function createComponentHledaniKvizuForm() {
		$form = new Form();
		$form->setMethod('POST');
		$form->addText("dotaz")
			->setDefaultValue("hledej uživatelské kvízy")
			->setAttribute("onclick","this.value=''");
		$form->addSubmit("submit", "hledej");
		$form->onSuccess[] = callback($this, "hledaniKvizuFormSubmitted");
		return $form;
	}
	
	public function hledaniKvizuFormSubmitted(Form $form) {
		$form->values->dotaz;
		$adresa = $this->link("VysledkyHledaniKvizu:default", array("dotaz" => $form->values->dotaz));
		$this->redirectUrl($adresa);
	}
	
	public function handleSingOut() {
		$this->getUser()->logout();
		$this->flashMessage("Byli jste úspěšně odhlášeni.", 'success');
		$this->redirect("this");
	}
	
	
	/*protected function createComponentSignInForm()
	{
		$form = new Form();
		$form->addText('username', 'Uživatelské jméno:', 30, 20);
		$form->addPassword('password', 'Heslo:', 30);
		$form->addCheckbox('persistent', 'pamatovat so mně na tomto počítači');
		$form->addSubmit('login', 'Přihlásit se');
		$form->onSuccess[] = callback($this, 'singInFormSubmitted');
		return $form;
	}

	public function signInFormSubmitted(Form $form)
	{
		try {
			$user = $this->getUser();
			$values = $form->getValues();
			if ($values->presistent) {
				$user->setExpiration('+30 days', FALSE);
			}
			$user->login($values->username, $values->password);
			$this->flashMessage('Přihlášení bylo úspěšné.', 'success');
			$this->redirect('Homepage:');
		}
		catch(NS\AutenthicationException $e) {
			$form->addError('Neplatné uživatelksé jméno nebo heslo.');
		}
	}*/
}
