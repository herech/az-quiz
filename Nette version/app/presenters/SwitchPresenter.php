<?php

use Nette\Application\UI\Form;

class SwitchPresenter extends BasePresenter {

protected function createComponentSwitchForm() {
	$form = new Form();
	$pole = array("on" => "On", "off" => "Off");
	$form->addRadioList("switch", "Switch", $pole);
	$form->addSubmit("submit","Přepni")
		->setAttribute("style","margin-top:10px;");
	$form->onSuccess[] = callback($this, "switchFormSubmitted");
	return $form;
}

public function switchFormSubmitted(Form $form) {
	setcookie ("jeZapnutySpecialniMod", "", time() - 3600, '/');
	if ($form->values->switch == "on")
		setcookie("jeZapnutySpecialniMod", "ano", time()+3600, '/');
	else 
		setcookie("jeZapnutySpecialniMod", "ne", time()+3600);
	$this->flashMessage("Přepnuto", "success");
	$this->redirect("this");
}
}
