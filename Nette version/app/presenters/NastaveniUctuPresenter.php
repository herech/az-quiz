<?php

use Nette\Application\UI\Form;

class NastaveniUctuPresenter extends BasePresenter {

	
	protected function createComponentZmenitHesloForm() {
		$form = new Form();
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addPassword('passwordOld', "Staré heslo:")
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password-old1')
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired('Zadejte Heslo!');
		$form->addPassword('passwordNew', "Nové heslo:")
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password-new1')
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired('Zadejte nové heslo!');
		$form->addPassword('passwordNewCheck', 'Nové heslo (potvrzení):')
				->setAttribute('autocomplete', 'off')
				->setAttribute('id', 'password-new-check1')
				->setRequired('Potvrďte heslo!')
				->addRule(Form::EQUAL, 'Zadaná hesla nejsou stejná!', $form['passwordNew']);
		
		$form->addSubmit('submit', 'Změň heslo');
		$form->onSuccess[] = callback($this, 'zmenitHesloFormSubmitted');
		return $form;
	}
	
	public function zmenitHesloFormSubmitted(Form $form) {
		if ($this->getUser()->isLoggedIn()) {
			$user = $this->getUser();
			if ($user->isLoggedIn()) {
				$uzivatel = new Uzivatel();
				if ($uzivatel->ZmenHeslo($user->id, $form->values->passwordOld, $form->values->passwordNew))
					$this->flashMessage("Vaše heslo bylo úspěšně změněno.","success");
				else 
					$this->flashMessage("Heslo nemohlo být změněno, pravděpodobně jste zadali nesprávně současné heslo.","error");
				$this->redirect('this');
			}
		}
	}
	
	protected function createComponentZmenitEmailForm() {
		$form = new Form();
		$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addPassword("passwordCurrent","Vaše současné heslo:")
				->setRequired("Zadejte vaše současné heslo!")
				->setAttribute("id","password-current")
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setAttribute("autocomplete","off");
		$form->addText("emailNew","Nová emailová adresa:")
				->setRequired("Zadejte vaši novou emailovou adresu!")
				->addRule(Form::EMAIL, 'Zadejte platný email!')
				->setAttribute("type", "email")
				->setAttribute("id","email-new")
				->setAttribute("autocomplete","off");
		$form->addSubmit("submit","Změň email");
		$form->onSuccess[] = callback($this, "zmenitEmailFormSubmitted");
		return $form;
	}
	
	public function zmenitEmailFormSubmitted(Form $form) {
		if ($this->getUser()->isLoggedIn()) {
			$user = $this->getUser();
			if ($user->isLoggedIn()) {
				$uzivatel = new Uzivatel();
				if ($uzivatel->ZmenEmail($user->id, $form->values->passwordCurrent, $form->values->emailNew))
					$this->flashMessage("Váš email byl úspěšně změněn.","success");
				else 
					$this->flashMessage("Email nemohl být změněn, pravděpodobně jste zadali nesprávně současné heslo.","error");
				$this->redirect('this');
			}
		}
	}
	public function renderDefault() {
		$user = $this->getUser();
		if ($user->isLoggedIn()) {
			$uzivatel = new Uzivatel();
			$this->template->uzivatel = $uzivatel->VratUzivatelskeUdaje($user->id);
		}
	}
	
} 	