<?php

class ConfirmNewPasswordPresenter extends BasePresenter {

	public function actionConfirm($email, $password_token, $password) {
			$uzivatel = new Uzivatel();
			if ($uzivatel->PotvrdNastaveniNovehoHesla($email,$password_token,$password))
				$this->flashMessage("Vaše heslo bylo úspěšně změněno!","success");
			else 
				$this->flashMessage("Vaše heslo nemohlo být změněno, platnost tokenu zřejmě vypršela","error");
			$this->redirect("Homepage:");
	}

} 