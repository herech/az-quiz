<?php 

class Prihlaseni {

	protected static $email;
	protected static $password;
	
	protected static $databaze;
	protected static $userId;
	
	public static function NaplnTridniVlastnosti() {
		
		if (isset($_POST['email'])) {		
			
			self::$email = $_POST['email'];
			self::$password = $_POST['password'];
			self::$databaze = new Databaze();
			
			return true;		
		}
		else {
			return false;
		}
	
	}

	public static function OverUdajeZadaneUzivatelem() {
		
		if (!(self::NaplnTridniVlastnosti()) || (!(ValidacniTrida::OverKontrolniSoucet()))) {
			return false;
		}
		
		if (self::OverPrihlasovaciUdajeUzivatele()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static function OverPrihlasovaciUdajeUzivatele() {
		
		if (self::$databaze->OverPrihlasovaciUdajeUzivatele(self::$email,self::$password)) {
			self::$userId = self::$databaze->VratUserId();
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public static function PrihlasUzivatele() {
		Sessions::NastavSessionPrihlaseni(self::$userId);
	}
}

?>