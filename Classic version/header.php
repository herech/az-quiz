		<div class="horni-pruh"></div>
		<div id="stranka_zahlavi">
			<div class="obal-hornidiv">
				<div id="titulek">
					<h1><a href="<?php echo Server::VratDomenu(); ?>" style="color:inherit;text-decoration:none;">Az kvíz</a></h1>
					<h2>by Honza Herec</h2>
				</div>
				<div id="flowers">
				</div>
				<div style="clear:both;">
				</div>
			</div>
		</div>
		<div class="hledejKvizy">
			<div class="obalNaKvizy">
					<form action="vysledky-hledani.php" method="post"><input type="text" name="dotaz" value="hledej uživatelské kvízy" onclick="this.value=''"><input type="submit" value="hledej" <?php if (!Uzivatel::JePrihlasen()) echo "disabled=\"true\""; ?>></form>
			</div>
		</div>
		<?php 
		$server = new Server();
		if ($server->MuzemeZpracovatParametryVUrl()): 
			$server->VyhodnotParametryVUrl();
		
		?>
		<div class="obal-telo" id="oznam-vysledek-akce" style="background:<?php echo $server->VratBarvu(); ?>;padding:10px;">
		<?php echo $server->ZobrazZpravu(); ?>
		</div>
		<?php else: ?>
		<?php if ((!Uzivatel::JePrihlasen()) && (preg_match("/index.php/", $_SERVER['SCRIPT_NAME']))): ?>
		<div class="obal-telo" style="padding:10px;width: 1004px; background: #ABDCEC;line-height: 1.5;font-size: 16px;">
			Vítejte na stránkách online (vyukové) hry az kvíz, kde můžete az kvízy tvořit, upravovat, spouštět a vyhledávat. <br />Nejprve se ale musíte registrovat. Pokud jste již registrování, můžete se přihlásit a začít tuto aplikaci používat. <br /> Pro používání této aplikace je nutné mít zapnutý javascript a pokud chcete mít i lepší vzhled doporučuji použít jiný prohlížeč než IE.
		</div>
		<?php endif; ?>
		<?php endif; ?>
		<div class="obal-telo">
		