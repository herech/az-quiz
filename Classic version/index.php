<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<div id="obsah">
					<h1>Vítejte</h1>
					<div id="prihlaseni-welcome"><p>Vítejte ve svém účtu na online vědomostní hře az kvíz. Tato hra slouží především k vyukovým a vzdělávacím účelům. </p>
					<p class="odstavec-welcome"><strong>Nástin hry v několika větách:</strong></p>

						<ul>
						<p><li>Hra má podobnou koncepci jako její populární televizní předloha.</li></p>
						<p><li>Je určena pro 2 hráče/skupiny – dále jen hráči. Každý má svou barvu – první hráč žlutou a druhý hráč červenou.</li></p>
						<p><li>Základním úkolem každého hráče je propojit pomocí políček jeho barvy všechny tři strany trojúhelníka, který tvoří az kvíz. Hráč, kterému se toto podaří vyhrává.</li>
						<p><li>Políčko vaší barvy získáte pokud správně odpovíte na otázku, nebo pokud vyberete správnou možnost z možností, které se k otázce vztahují (záleží na typu az kvízu). Pokud  vaše odpověď bude nesprávná, políčko, které jste si zvolili bude zbarveno šedě a je možné o něj losovat.</li></p> 
						<p><li>Losování probíhá tak, že se na obrazovce střídají 2 různé obrázky. Když se objeví dva stejné vedle je nutné zmáčknou příslušnou kávesu. Pohotovější hráč obdrží políčko. Předčasné zmáčknutí klávesy automaticky připíše políčko druhému hráči.</li></p> 								
						</ul>
						<p class="odstavec-welcome"><strong>Nástin možností těchto stránek:</strong></p>
						<ul>
						<p><li>Na těchto stránkách můžete kvízy vytvářet, upravovat, mazat a samozřejmě také spouštět.</li></p> 
						<p><li>Kvízy, které jsou označeny jako veřejné je také možné vyhledat a použít pro vlastní potřebu.</li></p>
						<p><li>Můžete tvořit buď kvízy, kde se ke každé otázce vztahují 3 možnosti a hráči pak vybírají tu správnou (pracnější), nebo kvízy bez možností, kde uvedete jen správnou odpověď (méně pracné). </li></p>
						<p><li>Pokud bude potřeba můžete také změnit své uživatelské údaje.</li></p>
						</ul>
					</div>
					
			</div>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
	</body>
</html>
