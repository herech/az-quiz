<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<div id="obsah">
					<h1>Přehled mých kvízů</h1>
					<div class="obsah-vytvoreni-kvizu">

							<?php
							if (Uzivatel::ExistujiVytvoreneKvizy()) {
								$kvizy = Uzivatel::VratSeznamKvizu();
								for ($i = 0;$i < count($kvizy);$i++): ?>
								<div style="margin-bottom: 10px; border: 2px solid #ABCBD0;background:#C8DEE2;border-radius: 15px;">
									<a href="<?php echo Server::VratDomenu() . "/az-kviz.php?idKvizu=" . $kvizy[$i]["id"] . "&nazevKvizu=" . urlencode($kvizy[$i]["name"]); ?>" style="text-decoration:none;"><h2 style="color: #0032AF;text-decoration:none;border-bottom: 2px dashed #85979A;padding-bottom: 10px;"><?php echo $kvizy[$i]["name"]; ?></h2></a>
									<p>
									<a href="upravit-kviz.php?idKvizu=<?php echo $kvizy[$i]["id"]; ?>"><img src="img/Edit24.png" style="margin-top: 10px;margin-left: 10px;"></a><a href="smaz-kviz.php?idKvizu=<?php echo $kvizy[$i]["id"]; ?>" class="smazat-kviz"><img src="img/Delete24.png" style="margin-top: 10px;margin-left: 10px; margin-right: 20px;"></a>
									<span> Kvíz vytvořen: <?php $nahrada = preg_replace("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) (.*)$/","\\3. \\2. \\1 v \\4",$kvizy[$i]["date"]); 
									echo preg_replace(array("/^0/","/\. 0/"),array("",". "),$nahrada);
									?></span>
									</p>
								</div>
							<?php endfor;
							}
							else { 
								echo "Zatím nemáte vytvořené žádné kvízy.";
							} ?>
							
					</div>
			</div>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			<?php if ($_GET['odhlasen'] == "ano") echo "Jsi odhlašen!"; ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
		<script type="text/javascript">
		$(function() {
			$(".smazat-kviz").click(function(prevent) {
				potvrdSmazani = window.confirm("Opravdu chcete smazat tento kvíz?");
				if (potvrdSmazani)
					return true;
				else {
					prevent.preventDefault();
					return false;
				}
			});
		});
		</script>
	</body>
</html>