<?php 
class Vyhledavani {	
	
	protected $dotaz;
	protected $databaze;
	
	public function __construct($dotaz) {
		$this->dotaz = $dotaz;
		$this->databaze = new Databaze();
	} 
	
	public function VratPocetVysledku() {
		$pocetVysledku = $this->databaze->VratPocetVysledkuProDotaz($this->dotaz);
		return $pocetVysledku;
	}
	
	public function VratSeznamKvizu() {
		$seznamKvizu = $this->databaze->VratSeznamKvizuProDotaz($this->dotaz);
		return $seznamKvizu;
	}
	
}
?>