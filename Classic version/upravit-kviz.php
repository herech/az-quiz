<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<div id="obsah">
					<?php if (($_GET['idKvizu'] == "") || (!Kviz::ExistujeKviz($_GET['idKvizu']))): ?>
								<div style="text-align: center;color:#BF0000; width: 100%;">
								<strong>Tento kvíz buď neexistuje, nebo nemáte oprávnění jej měnit!</strong>
								</div>
							<?php
							else:
								$vlastnostiKvizu = Kviz::VratVlastnostiKvizu($_GET['idKvizu']);
								$otazky = Kviz::VratOtazkyKvizu($_GET['idKvizu']);
								$odpovedi = Kviz::VratOdpovediKvizu($_GET['idKvizu']);
								$idKvizu = $_GET['idKvizu'];
					?>
					<h1>Upravení vašeho kvízu</h1>
					<div class="obsah-vytvoreni-kvizu">
						<form action="uprav-kviz.php" method="post">
							<div style="margin-bottom:20px;">
								<h2>Název vašeho kvízu: </h2>
								<input type="text" id="nazev-kvizu" name="nazev-kvizu" value="<?php if (isset($_COOKIE["nazevKvizu"]) && ($_COOKIE["nazevKvizu"] != "")){ echo $_COOKIE["nazevKvizu"];} else { echo $vlastnostiKvizu["name"]; }?>">
								<label for="verejny-kviz" style="margin: 20px 20px 20px 40px;">Veřejný kvíz</label>
								<input type="checkbox" id="verejny-kviz" name="public" value="ano" <?php if (isset($_COOKIE["public"]) && ($_COOKIE["public"] != "")) { echo "checked"; } else {$public = ($vlastnostiKvizu["public"] == "ano") ? "checked" : ""; echo $public;} ?> style="display:inline;">
								<div id="zluta-barva">
									<ul><li>veřejný kvíz bude zobrazen ve výsledcích vyhledávání (pouze pro přihlášené uživatele) a bude dostupný po zadání jeho url adresy (opět pouze pro přihlášené uživatele)</li>
									</ul>
									</div>
								</div>
							<hr />
							<?php if (Databaze::ObsahujeKvizMoznosti($idKvizu)):?>
							<h3>28 otázek + pro každou 3 možnosti:</h3>
							<div id="zluta-barva">
							<ul><li>čísla otázek nemají spojitost s čísly políček kvízu</li>
							<li>zelené políčko označuje správnou možnost pro danou otázku, červené políčka označují nesprávné možnosti</li></ul>
							</div>
							<?php else: ?>
							<h3>28 otázek se správnými možnostmi:</h3>
							<div id="zluta-barva">
							<ul><li>čísla otázek nemají spojitost s čísly políček kvízu</li>
							<li>zelené políčko označuje správnou možnost pro danou otázku</li></ul>
							</div>
							<?php 
							endif;
						
							if (isset($_COOKIE["nazevKvizu"]) && ($_COOKIE["nazevKvizu"] != "")):
								$cookieOtazky = explode("||", $_COOKIE["otazky"]);
								$cookieOdpovedi = explode("||", $_COOKIE["otazky"]);
								if (Databaze::ObsahujeKvizMoznosti($idKvizu)):
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky" value="<?php echo $cookieOtazky[($i - 1)]; ?>">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<?php  for ($j = 1;$j <= 3;$j++): ?>
										<input type="text" name="odpovedi[]" class="odpovedi<?php if (($j % 3) == 1) echo " spravna-odpoved";?>" value="<?php echo $cookieOdpovedi[($j - 1)]; ?>">
										<?php endfor;
									endfor;
								else:
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky" value="<?php echo $cookieOtazky[($i - 1)]; ?>">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<input type="text" name="odpovedi[]" class="odpovedi<?php echo " spravna-odpoved";?>" value="<?php echo $cookieOdpovedi[($i - 1)]; ?>">
										<?php 
									endfor;
								endif;
							else:
								if (Databaze::ObsahujeKvizMoznosti($idKvizu)):
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky" value="<?php echo $otazky[($i - 1)]; ?>">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<?php  for ($j = 1;$j <= 3;$j++): ?>
										<input type="text" name="odpovedi[]" class="odpovedi<?php if (($j % 3) == 1) echo " spravna-odpoved";?>" value="<?php echo $odpovedi[($i * 3 - 4 + $j)]; ?>">
										<?php endfor;
									endfor;
								else:
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky" value="<?php echo $otazky[($i - 1)]; ?>">
										<div class="cislo"><?php echo $i . ". "; ?></div>										
										<input type="text" name="odpovedi[]" class="odpovedi<?php echo " spravna-odpoved";?>" value="<?php echo $odpovedi[($i - 1)]; ?>">
										<?php 
									endfor;
								endif;
							endif;	
							?>
							<input type="hidden" name="idKvizu" value="<?php echo $_GET['idKvizu']; ?>">
							<input type="hidden" name="nazevPredchoziStranky" value="upravit-kviz.php">
							<input type="submit" value="Uprav kvíz">
							</form>
					</div>
					<?php endif; ?>
			</div>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			<?php if ($_GET['odhlasen'] == "ano") echo "Jsi odhlašen!"; ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
		<script type="text/javascript">
		$(function() {
			/*$('div.obsah-vytvoreni-kvizu input[type="text"]').each(function() {
				
				$(this).val("m");

			});*/
			
			$('form[action="uprav-kviz.php"]').submit(function(submit) {
			$('div.obsah-vytvoreni-kvizu input[type="text"]').each(function() {
				
				if ($(this).val() == "") {
					alert("Nejsou vyplněna všechna políčka, vyplňte je prosím");
					submit.preventDefault();
					return false;
				}
				else {
					return true;
				}

			});
			});
		});
		</script>
	</body>
</html>