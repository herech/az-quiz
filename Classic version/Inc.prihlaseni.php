<div id="obsah-pro-neprihlasene">
				<div id="obal-registrace">
					<h2>Registrace</h2>
					<form action="registrace.php" id="registrace" method="post">
						<label for="email1"><strong>Váš email</strong> (bude sloužit jako vaše přihlašovací jméno):</label>
						<input type="email" name="email" id="email1" required="true">
						<div id="neplatny-email" style="display:none;color:red;font-size:16px;">Neplatný email!</div >
						<label for="password1"><strong>Vaše heslo</strong> (doporučeno min. 10 znaků):</label>
						<input type="password" name="password" id="password1" required="true" autocomplete="off">
						<label for="password2"><strong>Vaše heslo</strong> (pro ověření):</label>
						<input type="password" name="password1" id="password2" required="true" autocomplete="off"> 
						<div  id="hesla-stejne">Hesla jsou stejné</div ><div  id="hesla-nestejne">Hesla nejsou stejné!</div>
						<div><label><strong>Vyplňte kontrolní součet</strong> (ochrana proti botům)</label></div>
						<div><input type="text" name="inputPrvniNahodneCislo" disabled="disabled" class="inputPrvniNahodneCislo" style="display:inline;width:50px"> + <input type="text" name="inputDruheNahodneCislo" disabled="disabled" class="inputDruheNahodneCislo" style="display:inline;width:50px"> = <input type="text" name="inputVysledekSouctu" class="inputVysledekSouctu" style="display:inline;width:50px">
						<input type="hidden" name="inputHiddenVysledekSouctu" class="inputHiddenVysledekSouctu">
						</div>
						<input type="submit" value="Registrovat">
					</form>
				</div>
				<div id="obal-prihlaseni">
					<h2>Přihlášení</h2>
					<form action="prihlaseni.php" method="post" id="prihlaseni">
						<label for="email2"><strong>Vaše přihlašovací jméno:</strong></label>
						<input type="email" name="email" id="email2">
						<label for="password3"><strong>Vaše heslo:</strong></label>
						<input type="password" name="password" id="password3" autocomplete="off">
						<div><label><strong>Vyplňte kontrolní součet</strong> (ochrana proti botům)</label></div>
						<div><input type="text" name="inputPrvniNahodneCislo" class="inputPrvniNahodneCislo" disabled="disabled" style="display:inline;width:50px"> + <input type="text" name="inputDruheNahodneCislo" disabled="disabled" class="inputDruheNahodneCislo" style="display:inline;width:50px"> = <input type="text" name="inputVysledekSouctu" class="inputVysledekSouctu" style="display:inline;width:50px">
						<input type="hidden" name="inputHiddenVysledekSouctu" class="inputHiddenVysledekSouctu">
						</div>
						<input type="submit" value="Přihlásit">
						<a href="#" id="odkaz-zapomenute-heslo">Zapoměli jste heslo?</a>
					</form>
					<h2 id="nadpis-zapomenute-heslo">Zapomenuté heslo</h2>
					<form action="zapomenute-heslo.php" method="post" id="zapomenute-heslo">
						<label for="email3"><strong>Váš email:</strong></label>
						<input type="email" name="email" id="email3">
						<label for="password"><strong>Vaše nové heslo:</strong></label>
						<input type="password" name="password" id="password4" autocomplete="off">
						<div><label><strong>Vyplňte kontrolní součet</strong> (ochrana proti botům)</label></div>
						<div><input type="text" name="inputPrvniNahodneCislo" class="inputPrvniNahodneCislo" disabled="disabled" style="display:inline;width:50px"> + <input type="text" name="inputDruheNahodneCislo" disabled="disabled" class="inputDruheNahodneCislo" style="display:inline;width:50px"> = <input type="text" name="inputVysledekSouctu" class="inputVysledekSouctu" style="display:inline;width:50px">
						<input type="hidden" name="inputHiddenVysledekSouctu" class="inputHiddenVysledekSouctu">
						</div>
						<input type="submit" value="Potvrď">
					</form>
				</div>
				<div style="clear:both"></div>
			</div>