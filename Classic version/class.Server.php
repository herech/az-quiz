<?php 
class Server {

	protected $poleKladnychZprav;
	protected $poleZapornychZprav; 

	protected $barva;
	protected $zprava;
		
	public function __construct() {
		$this->poleKladnychZprav = array("odhlasen=ano" => "Byli jste úspěšně odhlášeni.", "registrovan=ano" => "Byli jste úspěšně zaregistrováni.", "noveHesloNastaveno=ano" => "Vaše nové heslo bylo úspěšně nastaveno.", "noveHesloOdeslanKPotvrzeni=ano" => "Na uvedený email byla zaslána žádost o potvrzení změny hesla.","novyKvizVytvoren=ano" => "Váš kvíz byl úspěšně vytvořen","kvizSmazan=ano" => "Váš kvíz byl úspěšně odstraněn.","kvizUpraven=ano" => "Váš kvíz byl úspěšně upraven.","emailZmenen=ano" => "Váš email byl úspěšně změněn.", "hesloZmeneno=ano" => "Vaše heslo bylo úspěšně změněno."); 
		$this->poleZapornychZprav = array("prihlasen=ne" => "Bohůžel jste nemohli být přihlášeni. Zkontrolujte vaše přihlašovací údaje.", "registrovan=ne" => "Bohužel jste nemohli být zaregistrováni. Uživatel s daným emailem buď existuje, nebo máte vypnutý javascript ve svém prohlížeči.", "noveHesloNastaveno=ne" => "Nové heslo nemohlo být nastaveno. Váš odkaz na jeho změnu byl neplatný.", "noveHesloOdeslanKPotvrzeni=ne" => "Žádost o potvrzení změny hesla nemohla být odeslána, daný email pravděpodobně neexistuje.","novyKvizVytvoren=ne" => "Daný kvíz nemohl být vytvořen. Pravděpodobně jste nevyplnili všechna požadovaná políčka.","kvizSmazan=ne" => "Daný kvíz nemohl být smazán.","kvizUpraven=ne" => "Nepodařilo se upravit daný kvíz.", "emailZmenen=ne" => "Litujeme, ale váš email nemohl být změněn, pravděpodobně jste nezadali správné heslo.", "hesloZmeneno=ne" => "Bohužel vaše heslo nemohlo být změněno, pravděpodobně jste nezadali správně současné heslo."); 
		$this->barva = "";
		$this->zprava = "";
	}
	
	/*-------DYNAMICKÉ METODY-PRO ZOBRAZENÍ VÝSTUPU AKCE-----*/
	
	public function MuzemeZpracovatParametryVUrl() {
		foreach ($_GET as $index => $value) {
			if ((array_key_exists($index ."=". $value, $this->poleKladnychZprav)) xor (array_key_exists($index ."=". $value, $this->poleZapornychZprav))) {
				$parametr = $index ."=". $value;
				if(Uzivatel::JePrihlasen() && (($parametr == "odhlasen=ano") || ($parametr == "prihlasen=ne"))) {
					return false;
				}
				return true;
			}
		}
		return false;
	}
	
	public function VyhodnotParametryVUrl() {
		foreach ($_GET as $index => $value) {
			if (array_key_exists($index ."=". $value, $this->poleKladnychZprav)) {
				$this->zprava = $this->poleKladnychZprav[$index ."=". $value];
				$this->barva = "#9AE593";
			}
			else {
				$this->zprava = $this->poleZapornychZprav[$index ."=". $value];
				$this->barva = "#F07979";
			}
		}
		/* OLD
		if (array_key_exists($_SERVER["QUERY_STRING"], $this->poleKladnychZprav)) {
			$this->zprava = $this->poleKladnychZprav[$_SERVER["QUERY_STRING"]];
			$this->barva = "#9AE593";
		}
		else {
			$this->zprava = $this->poleZapornychZprav[$_SERVER["QUERY_STRING"]];
			$this->barva = "#F07979";
		}*/
	}
	public function ZobrazZpravu() {
		return $this->zprava;
	}
	public function VratBarvu() {
		return $this->barva;
	}
	
	
	/*-------STATICKÉ METODY------*/
	
	public static function VratDomenu() {
		if ($_SERVER['HTTPS'] == "" || $_SERVER['HTTPS'] == "off") {
			return "http://$_SERVER[HTTP_HOST]";
		}
		else {
			return "http://$_SERVER[HTTP_HOST]";
		}
	}

	public static function PresmerujUzivatele($adresa) {
		header("Location: $adresa");
	}
	
	public static function VratAktualniJmenoSkriptuSParametry() {
		if ($_SERVER['QUERY_STRING'] == "") {
			$jmenoSkriptuSParametry = $_SERVER['SCRIPT_NAME'] ;
		}
		else {
			$jmenoSkriptuSParametry = $_SERVER['SCRIPT_NAME'] ."?". $_SERVER['QUERY_STRING'];
		}
		return $jmenoSkriptuSParametry;
	}	
	public static function VratPosledniStrankuSProvedenouAkci() {
		if (isset($_COOKIE['posledniStrankaSProvedenouAkci'])) {
			return $_COOKIE['posledniStrankaSProvedenouAkci'];
		}
		else return false;
	}

}
?>