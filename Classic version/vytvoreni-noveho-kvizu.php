<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<div id="obsah">
					<h1>Vytvoření nového kvízu</h1>
					<div class="obsah-vytvoreni-kvizu">
						<form action="vytvor-novy-kviz.php" method="post">
							<div style="margin-bottom:20px;">
								<h2>Název vašeho kvízu: </h2>
								<input type="text" id="nazev-kvizu" name="nazev-kvizu" value="<?php if (isset($_COOKIE["nazevKvizu"]) && ($_COOKIE["nazevKvizu"] != "")){ echo $_COOKIE["nazevKvizu"];}?>">
								<label for="verejny-kviz" style="margin: 20px 20px 20px 40px;">Veřejný kvíz</label>
								<input type="checkbox" id="verejny-kviz" name="public" value="ano" checked="true" style="display:inline;">
								<div id="zluta-barva">
								<ul><li>veřejný kvíz bude zobrazen ve výsledcích vyhledávání (pouze pro přihlášené uživatele) a bude dostupný po zadání jeho url adresy (opět pouze pro přihlášené uživatele)</li>
								</ul>
								</div>
							</div>
							<hr/>
							<div id="mame-na-vyber-jaky-typ-kvizu-vytvorime" style="margin-top: 10px; text-align: center">
							<h3 style="text-align:left;margin-bottom: 30px;">Vyberte typ kvízu: </h3>
								<a href="#" id="click-kviz-bez-moznosti" style="background: #00638E;color:white;text-decoration:none; padding: 7px; border-radius: 7px;font-weight:bold;">Kvíz bez možností</a>
								  nebo  
								<a href="#" id="click-kviz-s-moznostmi" style="background: #009150;color:white;text-decoration:none; padding: 7px; border-radius: 7px;font-weight:bold;">Kvíz s možnostmi</a>
								<div id="seda-barva" style="margin-top: 30px;">
								<ul><li>Vaše volba typu kvízu je nevratná, dobře si tedy rozmyslete jaký typ kvízu chcete nyní vytvořit.</li>
								<li>Především záleží na tématu, které chcete zpracovat, na způsobu jakým jej chcete zpracovat a také na množství času a energie, kterou jste ochotni do této tvorby investovat. </li>
								<li>Pokud se s tvorbou kvízu „nechcete příliš párat“ doporučuji kvíz bez možností.</li>
								<li>Pokud si s kvízem chcete naopak trošku vyhrát doporučuji kvíz s možnostmi.</li>
								</ul>
								</div>

							</div>
							<div id="kviz-s-moznostmi" style="display:none;">
								<h3>Vytvořte 28 otázek + pro každou 3 možnosti:</h3>
								<div id="zluta-barva">
								<ul><li>čísla otázek nemají spojitost s čísly políček kvízu</li>
								<li>zelené políčko označuje správnou možnost pro danou otázku, červené políčka označují nesprávné možnosti</li></ul>
								</div> 
								<?php  
								if (isset($_COOKIE["nazevKvizu"]) && ($_COOKIE["nazevKvizu"] != "")):
									$cookieOtazky = explode("||", $_COOKIE["otazky"]);
									$cookieOdpovedi = explode("||", $_COOKIE["otazky"]);
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky" value="<?php echo $cookieOtazky[($i - 1)]; ?>">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<?php  for ($j = 1;$j <= 3;$j++): ?>
										<input type="text" name="odpovedi[]" class="odpovedi<?php if (($j % 3) == 1) echo " spravna-odpoved";?>" value="<?php echo $cookieOdpovedi[($j - 1)]; ?>">
										<?php endfor;
									endfor;
								else:
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<?php  for ($j = 1;$j <= 3;$j++): ?>
										<input type="text" name="odpovedi[]" class="odpovedi<?php if (($j % 3) == 1) echo " spravna-odpoved";?>">
										<?php endfor;
									endfor;
								endif;
								?>
								<input type="hidden" name="nazevPredchoziStranky" value="vytvoreni-noveho-kvizu.php">
								<input type="hidden" name="typKvizu" value="kvizSMoznostma">
								<input type="submit" value="Vytvoř nový kvíz">
							</div>
							<div id="kviz-bez-moznosti" style="display:none;">
								<h3>Vytvořte 28 otázek se správnými možnostmi:</h3>
								<div id="zluta-barva">
								<ul><li>čísla otázek nemají spojitost s čísly políček kvízu</li>
								<li>zelené políčko označuje správnou možnost pro danou otázku</li>
								</ul>
								</div> 
								<?php  
									for ($i = 1;$i <= 28;$i++): ?>
										<input type="text" name="otazky[]" class="otazky">
										<div class="cislo"><?php echo $i . ". "; ?></div>
										<input type="text" name="odpovedi[]" class="odpovedi spravna-odpoved">
										<?php
									endfor;
								?>
								<input type="hidden" name="nazevPredchoziStranky" value="vytvoreni-noveho-kvizu.php">
								<input type="hidden" name="typKvizu" value="kvizBezMoznosti">
								<input type="submit" value="Vytvoř nový kvíz">
							</div>
							</form>
					</div>
			</div>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			<?php if ($_GET['odhlasen'] == "ano") echo "Jsi odhlašen!"; ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
		<script type="text/javascript">
		$(function() {
			/*$('div.obsah-vytvoreni-kvizu input[type="text"]').each(function() {
				
				$(this).val("m");

			});*/
			$("#click-kviz-s-moznostmi").bind("click", function(kliknuti) {
				kliknuti.preventDefault();
				$("#mame-na-vyber-jaky-typ-kvizu-vytvorime").fadeOut("slow", function() {
					$("#kviz-bez-moznosti").remove();
					$("#kviz-s-moznostmi").fadeIn();
				});
			});
			$("#click-kviz-bez-moznosti").bind("click", function(kliknuti) {
				kliknuti.preventDefault();
				$("#mame-na-vyber-jaky-typ-kvizu-vytvorime").fadeOut("slow", function() {
					$("#kviz-s-moznostmi").remove();
					$("#kviz-bez-moznosti").fadeIn();
				});
			});
			
			$('form[action="vytvor-novy-kviz.php"]').submit(function(submit) {
			$('div.obsah-vytvoreni-kvizu input[type="text"]').each(function() {
				
				if ($(this).val() == "") {
					alert("Nejsou vyplněna všechna políčka, vyplňte je prosím.");
					submit.preventDefault();
					return false;
				}
				else {
					return true;
				}

			});
			});
		});
		</script>
	</body>
</html>