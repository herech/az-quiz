<?php 
class Uzivatel {
	
	public static function IdUzivatele() {
		
		$id = Sessions::VratIdUzivatele();
		return $id;
	}
	
	public static function PrihlasitSe() {
		if (Prihlaseni::OverUdajeZadaneUzivatelem()) {			
			Prihlaseni::PrihlasUzivatele();
			self::PrejitNaStrankuKdeBylaProvedenaAkce();
			/*PŮVODNÍ  self::PrejitNaUvodniStranku();*/
		}
		else {
			self::PrejitNaStrankuKdeBylaProvedenaAkce("prihlasen=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("prihlasen=ne"); */
		}
	}
	
	public static function JePrihlasen() {
		if (Sessions::JeNastavenaSessionPrihlaseni()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static function OdhlasitSe($prejitNaUvodniStranku) {
		if (self::JePrihlasen()) {
			Sessions::VycistiSession();
		}
		else {
			self::PrejitNaUvodniStranku();
		}
		if ($prejitNaUvodniStranku) {
			self::PrejitNaUvodniStranku("odhlasen=ano"); 
		}
	}
	
	public static function PrejitNaUvodniStranku($parametry="") {
		$parametry = ($parametry == "") ? "" : "?$parametry";
		$domenaServeru = Server::VratDomenu();
		Server::PresmerujUzivatele("$domenaServeru{$parametry}");
	}
	public static function PrejitNaStrankuKdeBylaProvedenaAkce($parametry="") {
		if (Server::VratPosledniStrankuSProvedenouAkci() == false) {
			self::PrejitNaUvodniStranku($parametry);
		}
		else {
			$domenaServeru = Server::VratDomenu();
			$posledniStrankaSProvedenouAkci = Server::VratPosledniStrankuSProvedenouAkci();
			if (preg_match("/\?.+/",$posledniStrankaSProvedenouAkci)) {
				$parametry = ($parametry == "") ? "" : "&$parametry";
				Server::PresmerujUzivatele("$domenaServeru"."$posledniStrankaSProvedenouAkci{$parametry}");
			}
			else {
				$parametry = ($parametry == "") ? "" : "?$parametry";
				Server::PresmerujUzivatele("$domenaServeru"."$posledniStrankaSProvedenouAkci{$parametry}");
			}
		}
	}
	
	public static function RegistrovatSe() {
		if (Registrace::OverUdajeZadaneUzivatelem() && Registrace::OverUnikatnostUzivatele()) {			
			Registrace::RegistrujUzivatele();
			self::PrejitNaStrankuKdeBylaProvedenaAkce("registrovan=ano");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("registrovan=ano");*/
		}
		else {
			self::PrejitNaStrankuKdeBylaProvedenaAkce("registrovan=ne");
			/*PŮVODNÍ self::PrejitNaUvodniStranku("registrovan=ne");*/
		}
	}
	public static function NastavNoveHeslo($potvrdNastaveniHesla) {
		if ($potvrdNastaveniHesla == true) {
			if (NastaveniNovehoHesla::OverPlatnostUdaju()) {			
				NastaveniNovehoHesla::NastavNoveHeslo();
				self::PrejitNaStrankuKdeBylaProvedenaAkce("noveHesloNastaveno=ano");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("noveHesloNastaveno=ano");*/
			}
			else {
				self::PrejitNaStrankuKdeBylaProvedenaAkce("noveHesloNastaveno=ne");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("noveHesloNastaveno=ne"); */
			}
		}
		else {
			if (NastaveniNovehoHesla::OverUdajeZadaneUzivatelem()) {			
				NastaveniNovehoHesla::OdesliPotvrzovaciEmail();
				self::PrejitNaStrankuKdeBylaProvedenaAkce("noveHesloOdeslanKPotvrzeni=ano");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("noveHesloOdeslanKPotvrzeni=ano");*/
			}
			else {
				self::PrejitNaStrankuKdeBylaProvedenaAkce("noveHesloOdeslanKPotvrzeni=ne");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("noveHesloOdeslanKPotvrzeni=ne");*/
			}
		}
	}
	
	public static function VytvorNovyKviz() {
		if (Kviz::OverUdajeZadaneUzivatelem()) {
			Kviz::VytvorNovyKviz();
			self::PrejitNaStrankuKdeBylaProvedenaAkce("novyKvizVytvoren=ano");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("novyKvizVytvoren=ano");*/
		}
		else {
			self::PrejitNaStrankuKdeBylaProvedenaAkce("novyKvizVytvoren=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("novyKvizVytvoren=ne");*/
		}
	}
	
	public static function ExistujiVytvoreneKvizy() {
	
		if (Kviz::ExistujiVytvoreneKvizy())
			return true;
		else 
			return false;
	
	} 
	
	public static function VratSeznamKvizu(){
	
		$seznamKvizu = Kviz::VratSeznamKvizu();
		return $seznamKvizu;
		
	} 	
	public static function SmazKviz(){
	
		if (Kviz::SmazKviz())
			self::PrejitNaStrankuKdeBylaProvedenaAkce("kvizSmazan=ano");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("kvizSmazan=ano"); */	
		else 
			self::PrejitNaStrankuKdeBylaProvedenaAkce("kvizSmazan=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("kvizSmazan=ne");*/
		
	}
	public static function UpravKviz(){
	
		if (Kviz::UpravKviz())
			self::PrejitNaStrankuKdeBylaProvedenaAkce("kvizUpraven=ano");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("kvizUpraven=ano"); */
		else 
			self::PrejitNaStrankuKdeBylaProvedenaAkce("kvizUpraven=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("kvizUpraven=ne"); */
		
	}
	
	/*METODY DEKLAROVÁNY V TŘÍDĚ KVÍZ 
	------------------------------------------------
	public static function ExistujeKviz($idKvizu) {
	
		if (Kviz::ExistujeKviz($idKvizu))
			return true;
		else 
			return false;
	
	} 
	
	public static function VratVlastnostiKvizu($idKvizu){
	
		$vlastnostiKvizu = Kviz::VratVlastnostiKvizu($idKvizu);
		return $vlastnostiKvizu;
		
	} 	*/
	
	public static function VratDataDoOknaKvizu() {
		$vracenaData = Kviz::VratDataDoOknaKvizu();
		return $vracenaData;
	}
	
	public static function VratUzivatelskeUdaje() {
		$databaze = new Databaze();
		return $databaze->VratUzivatelskeUdaje();
	}
	
	public static function ZmenHeslo() {
		if ((isset($_POST['password-old'])) && (ValidacniTrida::OverHodnotyPOST()) && ($_POST['password-new'] == $_POST['password-new-check'])) {
			$databaze = new Databaze();
			if ($databaze->ZmenHeslo($_POST['password-old'], $_POST['password-new']))
				self::PrejitNaStrankuKdeBylaProvedenaAkce("hesloZmeneno=ano");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("hesloZmeneno=ano");*/
			else
				self::PrejitNaStrankuKdeBylaProvedenaAkce("hesloZmeneno=ne");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("hesloZmeneno=ne");*/
		}
		else {
			self::PrejitNaStrankuKdeBylaProvedenaAkce("hesloZmeneno=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("hesloZmeneno=ne");*/
		}
	}
	
	public static function ZmenEmail() {
		if ((isset($_POST['password-current'])) && (ValidacniTrida::OverHodnotyPOST()) && (ValidacniTrida::OverEmailUzivatele($_POST['email-new']))) {
			$databaze = new Databaze();
			if ($databaze->ZmenEmail($_POST['password-current'], $_POST['email-new']))
				self::PrejitNaStrankuKdeBylaProvedenaAkce("emailZmenen=ano");
				/* PŮVODNÍ self::PrejitNaUvodniStranku("emailZmenen=ano");*/
			else
				self::PrejitNaStrankuKdeBylaProvedenaAkce("emailZmenen=ne");
				/* PŮVPDNÍ self::PrejitNaUvodniStranku("emailZmenen=ne"); */
		}
		else {
			self::PrejitNaStrankuKdeBylaProvedenaAkce("emailZmenen=ne");
			/* PŮVODNÍ self::PrejitNaUvodniStranku("emailZmenen=ne"); */
		}
	}
	
}
?>