<?php
class Databaze {
	
	protected $userId;
	/*------------------------------METODY PO VYTVOŘENÍ NOVÉHO OBJEKTU DATABÁZE-------------------------------------*/
	
	public function __construct() {
		$this->VytvorSpojeni();
		$this->VyberDatabazi();
		$this->NastavKodovani();
	}
	
	public function VytvorSpojeni() {
		mysql_connect(Konstanty::DB_HOST, Konstanty::DB_USER, Konstanty::DB_PASSWORD);
	}
	
	public function VyberDatabazi() {
		if (!mysql_select_db(Konstanty::DB_NAME))
			die("Nezdařilo se připojení k databázi.");
	}
	
	public function NastavKodovani() {
		mysql_set_charset("utf8");
	}
/*------------------------------METODY V PROCESU REGISTRACE-------------------------------------*/	

	public function OverUnikatnostUzivatele($login) {
		$login = mysql_real_escape_string($login);
		$dotaz = mysql_query("SELECT * FROM az_users WHERE login = '$login'");
		if (mysql_num_rows($dotaz))
			return false;
		else 
			return true;
	}
	
	public function RegistrovatUzivatele($login, $password, $email) {
		$login = mysql_real_escape_string($login);
		$password = md5(mysql_real_escape_string($password));
		$email = mysql_real_escape_string($email);
		
		$dotaz = mysql_query("INSERT INTO az_users (login, password, email, date) VALUES ('$login', '$password', '$email', CURRENT_TIMESTAMP())");
		if (!(mysql_affected_rows() == 1))
			throw new Exception("Nepodařilo se zaregistrovat");
	}
/*------------------------------METODY V PROCESU PŘIHLÁŠENÍ-------------------------------------*/		

	public function OverPrihlasovaciUdajeUzivatele($login, $password) {
		$login = mysql_real_escape_string($login);
		$password = md5(mysql_real_escape_string($password));
		$dotaz = mysql_query("SELECT * FROM az_users WHERE login = '$login' AND password = '$password'");
		if (mysql_num_rows($dotaz)) {
			$userId = mysql_result(mysql_query("SELECT id FROM az_users WHERE login = '$login' AND password = '$password'"),0);
			$this->userId = $userId;
			return true;
		}
		else 
			return false;
	}
	
	public function VratUserId() {
		return $this->userId;
	}
/*------------------------------METODY V PROCESU NASTAVENÍ NOVÉHO HESLA-------------------------------------*/		

	public function OverExistenciEmailu($email) {
		$email = mysql_real_escape_string($email);		
		$dotaz = mysql_query("SELECT * FROM az_users WHERE email = '$email'");
		
		if (mysql_num_rows($dotaz)) 
			return true;
		else return false;
	}
	
	public function NastavToken($email, $token) {
		$email = mysql_real_escape_string($email);	
		$token = mysql_real_escape_string($token);
		mysql_query("UPDATE az_users SET password_token = '$token',password_token_generated = NOW() WHERE email = '$email'");	
	}
	
	public function OverPlatnostUdaju($email, $token) {
		$email = mysql_real_escape_string($email);	
		$token = mysql_real_escape_string($token);
		$dotaz = mysql_query("SELECT * FROM az_users WHERE email = '$email' AND password_token = '$token' AND password_token_generated >= NOW() - INTERVAL 1 DAY");
		
		if (mysql_num_rows($dotaz)) 
			return true;
		else return false;
	}
	
	public function NastavNoveHeslo($password, $email) {
		$password = mysql_real_escape_string($password);
		$email = mysql_real_escape_string($email);
		mysql_query("UPDATE az_users SET password_token_generated = NULL, password_token = NULL, password = '$password' WHERE email = '$email'");
		/*možná bude zlobitif (!(mysql_affected_rows() == 1))
			throw new Exception("Nepodařilo se nastavení nového hesla");*/
	}
/*------------------------------METODY V PROCESU VYTVOŘENÍ NOVÉHO KVÍZU-------------------------------------*/	

	public function VytvorNovyKviz($nazevKvizu, $public, $otazky, $odpovedi, $typKvizu) {
		$nazevKvizu = mysql_real_escape_string($nazevKvizu);
		$public = mysql_real_escape_string($public);
		
		$pocetOtazek = count($otazky);	
		for ($i = 0; $i < $pocetOtazek; $i++) {
			$otazky[$i] = mysql_real_escape_string($otazky[$i]);
		}
		
		$pocetOdpovedi = count($odpovedi);
		for ($i = 0; $i < $pocetOdpovedi; $i++) {
			$odpovedi[$i] = mysql_real_escape_string($odpovedi[$i]);
		}
		$idUzivatele = Uzivatel::IdUzivatele();
		
		if ($typKvizu == "kvizBezMoznosti") {
		
			mysql_query("INSERT INTO az_quizs (id_az_user, name, public, date, have_options) VALUES ('$idUzivatele', '$nazevKvizu', '$public', CURRENT_TIMESTAMP(), 'ne')");
			$idKvizu = mysql_insert_id();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				mysql_query("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES ('$idKvizu', '$idUzivatele', '$aktualniOtazka', '". ($i+1) ."')");
				$idOtazky = mysql_insert_id();

					$aktualniOdpoved = $odpovedi[$i];
					if (!mysql_query("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES ('$idKvizu', '$idOtazky', '$idUzivatele', '$aktualniOdpoved', 'ano', '1')"))
						die("Nepodařilo se uložit kvíz");
			}
		
		}
		else {
			
			mysql_query("INSERT INTO az_quizs (id_az_user, name, public, date) VALUES ('$idUzivatele', '$nazevKvizu', '$public', CURRENT_TIMESTAMP())");
			$idKvizu = mysql_insert_id();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				mysql_query("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES ('$idKvizu', '$idUzivatele', '$aktualniOtazka', '". ($i+1) ."')");
				$idOtazky = mysql_insert_id();
				
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					if (!mysql_query("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES ('$idKvizu', '$idOtazky', '$idUzivatele', '$aktualniOdpoved', '$spravnaOdpoved', '$poradiOdpovedi')"))
						die("Nepodařilo se uložit kvíz");
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
	}
/*------------------------------METODY V PROCESU ZOBRAZENÍ VLASTNÍCH KVÍZŮ-------------------------------------*/		
	
	public function ExistujiVytvoreneKvizy() {
		if (mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE id_az_user = '". Uzivatel::IdUzivatele() ."'"))) 
			return true;
		else 
			return false;
	}
	
	public function VratSeznamKvizu() {
		$seznamKvizu = array();
		$i = 0;
		$dotaz = mysql_query("SELECT id, name, date FROM az_quizs WHERE id_az_user = '". Uzivatel::IdUzivatele() ."' ORDER BY date DESC");
		
		while($radek = mysql_fetch_object($dotaz)) {
			$seznamKvizu[$i]["id"] = $radek->id;
			$seznamKvizu[$i]["name"] = $radek->name;
			$seznamKvizu[$i]["date"] = $radek->date;
			$i += 1;
			//array (array("id" => , "name" =>, "date" => ), array("id" => , "name" =>, "date" => ))
		}		
		return $seznamKvizu;
	}
/*------------------------------METODY V PROCESU ÚPRAVY KVIZU-------------------------------------*/		
	
	public function SmazKviz($idKvizu){
		$idKvizu = mysql_real_escape_string($idKvizu);
		mysql_query("DELETE FROM az_quizs WHERE id = '$idKvizu'");
		if (mysql_affected_rows() == 1) {
			mysql_query("DELETE FROM az_questions WHERE id_az_quiz = '$idKvizu'");
			mysql_query("DELETE FROM az_answers WHERE id_az_quiz = '$idKvizu'");
			return true;
		}
		else 
			return false;
	}
	
	public function UpravKviz($idKvizu, $nazevKvizu, $public, $otazky, $odpovedi) {
		$idKvizu = mysql_real_escape_string($idKvizu);
		$nazevKvizu = mysql_real_escape_string($nazevKvizu);
		$public = mysql_real_escape_string($public);

		$pocetOtazek = count($otazky);	
		for ($i = 0; $i < $pocetOtazek; $i++) {
			$otazky[$i] = mysql_real_escape_string($otazky[$i]);
		}
		
		$pocetOdpovedi = count($odpovedi);
		for ($i = 0; $i < $pocetOdpovedi; $i++) {
			$odpovedi[$i] = mysql_real_escape_string($odpovedi[$i]);
		}
		$idUzivatele = Uzivatel::IdUzivatele();
		
		if (!mysql_query("UPDATE az_quizs SET name = '$nazevKvizu', public = '$public' WHERE id = '$idKvizu' AND id_az_user = '". Uzivatel::IdUzivatele() ."'"))
			return false;
		
		if (!self::ObsahujeKvizMoznosti($idKvizu)) {
		
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				mysql_query("UPDATE az_questions SET name = '$aktualniOtazka' WHERE id_az_quiz = '$idKvizu' AND order_number = '". ($i+1) ."'");
				$idOtazky = mysql_result(mysql_query("SELECT id FROM az_questions WHERE id_az_quiz = '$idKvizu' AND order_number = '". ($i+1) ."'"), 0);
					
				$aktualniOdpoved = $odpovedi[$i];
				if (!mysql_query("UPDATE az_answers SET name = '$aktualniOdpoved' WHERE id_az_quiz = '$idKvizu' AND id_az_question = '$idOtazky' AND order_number = '1'"))
					die("Nepodařilo se upravit kvíz");
						
			}
		}
		else {
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				mysql_query("UPDATE az_questions SET name = '$aktualniOtazka' WHERE id_az_quiz = '$idKvizu' AND order_number = '". ($i+1) ."'");
				$idOtazky = mysql_result(mysql_query("SELECT id FROM az_questions WHERE id_az_quiz = '$idKvizu' AND order_number = '". ($i+1) ."'"), 0);
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					if (!mysql_query("UPDATE az_answers SET name = '$aktualniOdpoved', right_answer = '$spravnaOdpoved' WHERE id_az_quiz = '$idKvizu' AND id_az_question = '$idOtazky' AND order_number = '$poradiOdpovedi'"))
						die("Nepodařilo se upravit kvíz");
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
		return true;
		
	}
	
	public function ExistujeKviz($idKvizu) {
	
		if (mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE id = '$idKvizu' AND id_az_user = '". Uzivatel::IdUzivatele() ."'")))
			return true;
		else 
			return false;
	
	} 
	
	public function VratVlastnostiKvizu($idKvizu){
		$idKvizu = mysql_real_escape_string($idKvizu);
		$dotaz = mysql_query("SELECT name, public FROM az_quizs WHERE id = '$idKvizu'");
		while($radek = mysql_fetch_object($dotaz)) {
			$vlastnostiKvizu["name"] = $radek->name;
			$vlastnostiKvizu["public"] = $radek->public;
		}	
		
		$vlastnostiKvizu = $this->OdescapujData($vlastnostiKvizu);
		
		return $vlastnostiKvizu;
		
	} 
	public function VratOtazkyKvizu($idKvizu) {
		$idKvizu = mysql_real_escape_string($idKvizu);
		$dotaz = mysql_query("SELECT name FROM az_questions WHERE id_az_quiz = '$idKvizu' AND id_az_user = '". Uzivatel::IdUzivatele() ."' ORDER BY order_number ASC");
		while($radek = mysql_fetch_object($dotaz)) {
			$otazkyKvizu[] = $radek->name;
		}	
		
		$otazkyKvizu = $this->OdescapujData($otazkyKvizu);
		
		return $otazkyKvizu;
	}
	
	public function VratOdpovediKvizu($idKvizu) {
		$idKvizu = mysql_real_escape_string($idKvizu);
		$dotaz = mysql_query("SELECT name FROM az_answers WHERE id_az_quiz = '$idKvizu' AND id_az_user = '". Uzivatel::IdUzivatele() ."' ORDER BY id ASC");
		while($radek = mysql_fetch_object($dotaz)) {
			$odpovediKvizu[] = $radek->name;
		}	
		
		$odpovediKvizu = $this->OdescapujData($odpovediKvizu);
		
		return $odpovediKvizu;
	}
/*------------------------------METODY V PROCESU SAMOTNÉ HRY AZ KVÍZ-------------------------------------*/		
	
	public function JeKvizVerejnyProSoucasnehoUzivatele($idKvizu) {
		$idKvizu = mysql_real_escape_string($idKvizu);
		if (mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE id = '$idKvizu' AND id_az_user = '". Uzivatel::IdUzivatele() ."'"))) 
			return true;
		else {
		if (mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE id = '$idKvizu' AND public = 'ano'"))) 
			return true;
		else 
			return false;
		}
	}
	
	public function VratDataDoOknaKvizu($idKvizu, $cisloOtazky) {
		if (self::JeKvizVerejnyProSoucasnehoUzivatele($idKvizu)) {
			$idKvizu = mysql_real_escape_string($idKvizu);
			$cisloOtazky = mysql_real_escape_string($cisloOtazky);
			$vracenaData["otazka"] = mysql_result(mysql_query("SELECT name FROM az_questions WHERE id_az_quiz = '$idKvizu' AND order_number = '$cisloOtazky'"), 0);
			$idOtazky = mysql_result(mysql_query("SELECT id FROM az_questions WHERE id_az_quiz = '$idKvizu' AND order_number = '$cisloOtazky'"), 0);
			
			if (self::ObsahujeKvizMoznosti($idKvizu)) {
			
				$vracenaData["odpoved1"] = mysql_result(mysql_query("SELECT name FROM az_answers WHERE id_az_question = '$idOtazky' AND order_number = '1'"), 0);
				$vracenaData["odpoved2"] = mysql_result(mysql_query("SELECT name FROM az_answers WHERE id_az_question = '$idOtazky' AND order_number = '2'"), 0);
				$vracenaData["odpoved3"] = mysql_result(mysql_query("SELECT name FROM az_answers WHERE id_az_question = '$idOtazky' AND order_number = '3'"), 0);
			}
			else {
				$vracenaData["odpoved1"] = mysql_result(mysql_query("SELECT name FROM az_answers WHERE id_az_question = '$idOtazky' AND order_number = '1'"), 0);
			}
			
			$vracenaData = $this->OdescapujData($vracenaData);
			
			return $vracenaData;
		}
		else return false;
	}
/*------------------------------METODY V PROCESU SPRÁVY ÚČTU-------------------------------------*/		
	
	public function VratUzivatelskeUdaje() {
		$dotaz = mysql_query("SELECT id, login, email, date FROM az_users WHERE id = '". Uzivatel::IdUzivatele() ."'");
		while ($radek = mysql_fetch_object($dotaz)) {
			$vraceneUdaje['id'] = $radek->id;
			$vraceneUdaje['login'] = $radek->login;
			$vraceneUdaje['email'] = $radek->email;
			$vraceneUdaje['date'] = $radek->date;
		}
		return $vraceneUdaje;
	}
	
	public function ZmenEmail($soucasneHeslo, $novyEmail) {
		//throw new Exception("111");
		$soucasneHeslo = mysql_real_escape_string($soucasneHeslo);
		$novyEmail = mysql_real_escape_string($novyEmail);
		mysql_query("UPDATE az_users SET email = '$novyEmail' WHERE id = '". Uzivatel::IdUzivatele() ."' AND password = '" . md5($soucasneHeslo) . "'");
		if (mysql_affected_rows() == 1)	
			return true;
		else return false;
	}
	
	public function ZmenHeslo($stareHeslo, $noveHeslo) {
		//throw new Exception("222");
		$stareHeslo = mysql_real_escape_string($stareHeslo);
		$noveHeslo = mysql_real_escape_string($noveHeslo);
		mysql_query("UPDATE az_users SET password = '". md5($noveHeslo) ."' WHERE id = '". Uzivatel::IdUzivatele() ."' AND password = '" . md5($stareHeslo) . "'");
		if (mysql_affected_rows() == 1)	
			return true;
		else return false;
	}
	
/*------------------------------METODY V PROCESU Vyhledávání-------------------------------------*/		
	public function VratPocetVysledkuProDotaz($dotaz) {
		$dotaz = mysql_real_escape_string($dotaz);
		if ($dotaz == "*") { 
			$pocetVysledku = mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE public = 'ano'"));
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$pocetVysledku = mysql_num_rows(mysql_query("SELECT * FROM az_quizs"));
		}
		else {
			$pocetVysledku = mysql_num_rows(mysql_query("SELECT * FROM az_quizs WHERE public = 'ano' AND name LIKE '%". $dotaz ."%'"));
		}
		return $pocetVysledku;
	}
	
	public function VratSeznamKvizuProDotaz($dotaz) {
		$i = 0;
		$dotaz = mysql_real_escape_string($dotaz);
		if ($dotaz == "*") {
			$dotaz = mysql_query("SELECT * FROM az_quizs WHERE public = 'ano'");
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$dotaz = mysql_query("SELECT * FROM az_quizs");
		}
		else {
			$dotaz = mysql_query("SELECT * FROM az_quizs WHERE public = 'ano' AND name LIKE '%". $dotaz ."%'");
			}
		while ($radek = mysql_fetch_object($dotaz)) {
			$seznamKvizu[$i]["id"] = $radek->id;
			$seznamKvizu[$i]["name"] = $radek->name;
			$login = mysql_result(mysql_query("SELECT login FROM az_users WHERE id = '". $radek->id_az_user ."'"), 0);
			$seznamKvizu[$i]["login"] = $login;
			$i += 1;
		}
		return $seznamKvizu;
	}
	
	public function OdescapujData($data) {
		$odescapovanaData = array();
		if (is_array($data)) {
		 foreach ($data as $index => $prvek) {
			$odescapovanaData[$index] = preg_replace("/\\\\+'/","'",$prvek);
		 }
		}
		else {
			$odescapovanaData = preg_replace("/\\\\+'/","'",$data);
		}
		return $odescapovanaData;
	}
	
	public function ObsahujeKvizMoznosti($idKvizu) {
		$idKvizu = mysql_real_escape_string($idKvizu);
		$obsahujeKvizMoznosti = mysql_fetch_row(mysql_query("SELECT have_options FROM az_quizs WHERE id = '$idKvizu'"));
		if ($obsahujeKvizMoznosti[0] == "ano") {
			return true;
		}
		else return false;
	}
}
?>