<?php 

class ValidacniTrida {

	protected static $inputVysledekSouctu;
	protected static $inputHiddenVysledekSouctu;
	
	public static function NaplnTridniVlastnosti() {		
			self::$inputVysledekSouctu = $_POST['inputVysledekSouctu'];
			self::$inputHiddenVysledekSouctu = $_POST['inputHiddenVysledekSouctu'];		
	}

	public static function OverKontrolniSoucet() {
		
		self::NaplnTridniVlastnosti();
		
		if ((empty(self::$inputHiddenVysledekSouctu)) || (self::$inputVysledekSouctu != self::$inputHiddenVysledekSouctu))
			return false;
		else 
			return true;
	}
	
	public static function OverEmailUzivatele($email) {
		
		if (preg_match("~^[^@]+@[^@]+\.[^@]{2,10}$~", $email))
			return true;
		else 
			return false;
	
	}
	
	public static function OverHodnotyPOST() {
		foreach ($_POST as $prvekPole) {
			if (is_array($prvekPole)) {
				foreach ($prvekPole as $prvekPole1) {
					if ($prvekPole1 == "")
						return false;
					else 
						return true;
				}
			}
			else {
				if ($prvekPole == "") 
					return false;
				else
					return true;
			}
		}
	
	}
}

?>