<?php 

class NastaveniNovehoHesla {

	protected static $email;
	protected static $password;
	
	protected static $databaze;
	protected static $token;
	
	public static function NaplnTridniVlastnosti($post, $get) {
		
		if ($post == true) {
			if ((isset($_POST['email'])) && (isset($_POST['password']))) {		
				
				self::$email = $_POST['email'];
				self::$password = $_POST['password'];	
				self::$databaze = new Databaze();	
				return true;
			}
			else {
				return false;
			}
		}
		else if ($get == true) {
			if ((isset($_GET['email'])) && (isset($_GET['password_token'])) && (isset($_GET['password']))){
				self::$email = $_GET['email'];
				self::$token = $_GET['password_token'];
				self::$password = $_GET['password'];
				self::$databaze = new Databaze();	
				return true;
			}
			else {
				return false;
			}
			
		}
	
	}

	public static function OverUdajeZadaneUzivatelem() {
		
		
		if ((!(self::NaplnTridniVlastnosti(true, false))) || (!(ValidacniTrida::OverKontrolniSoucet())) || (!(ValidacniTrida::OverEmailUzivatele(self::$email))) || (self::$password == ""))
			return false;
		
		if (self::OverExistenciEmailu()) {
			return true;
		}
		else 
			return false;
	}
	
	public static function OverExistenciEmailu() {
		
		if (self::$databaze->OverExistenciEmailu(self::$email)) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	protected static function NastavToken() {
		self::$token = md5(uniqId(mt_rand(), true));
		self::$databaze->NastavToken(self::$email, self::$token);
	}
	
	protected function mime_header_encode($text, $encoding = "utf-8") {
		return "=?$encoding?Q?" . imap_8bit($text) . "?=";
	}
	
	public static function OdesliPotvrzovaciEmail() {
		self::NastavToken();
		$adresa = Server::VratDomenu() . "/nastaveni-noveho-hesla.php?email=" . self::$email . "&password_token=" . self::$token . "&password=" . md5(self::$password); 
		$headers = "MIME-Version: 1.0"
		 . PHP_EOL . "From: az-kviz@set-new-password.cz" 
		 . PHP_EOL . "Content-Type: text/plain; charset=utf-8"
		 . PHP_EOL . "Content-Transfer-Encoding: 8bit";
		
		if (mail(self::$email, self::mime_header_encode("Zapomenuté heslo"), "Dobrý den," . PHP_EOL . "Požádali jste o nastavení nového hesla pro váš účet v online hře az kvíz. Jako nové heslo jste si zvolili: " . self::$password . ". K úspěšnému potvrzení změny hesla prosím následujte následující odkaz $adresa." . PHP_EOL . "Pokud jste nepožádali o změnu hesla, či dokonce ani nemáte vytvořen účet v této online hře, tak pouze někdo zneužil vaši emailovou adresu a prosím ignorujte tento mail.", $headers)) {
			Uzivatel::PrejitNaUvodniStranku("noveHesloOdeslanKPotvrzeni=ano");
		}
		else {
			Uzivatel::PrejitNaUvodniStranku("noveHesloOdeslanKPotvrzeni=ne");
		}
	}
	
	public static function OverPlatnostUdaju() {

		if (!(self::NaplnTridniVlastnosti(false, true))) {
			return false;
		}
		if (self::$databaze->OverPlatnostUdaju(self::$email, self::$token)) {
			return true;
		}
		else return false;
		
	}
	
	public static function NastavNoveHeslo() {
		self::$databaze->NastavNoveHeslo(self::$password, self::$email);
	} 
}

?>