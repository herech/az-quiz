CREATE TABLE `az_users` (
`id` MEDIUMINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`login` VARCHAR(100) NOT NULL,
`password` VARCHAR(100) NOT NULL,
`email` VARCHAR(100)NOT NULL,
`date` DATETIME NOT NULL,
password_token VARCHAR(100) NULL,
password_token_generated TIMESTAMP NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE = utf8_czech_ci;

CREATE TABLE `az_quizs` (
`id` MEDIUMINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`id_az_user` MEDIUMINT UNSIGNED NOT NULL,
`name` VARCHAR(200) NOT NULL,
`public` ENUM('ano','ne') NOT NULL,
`date` DATETIME NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE = utf8_czech_ci;

CREATE TABLE `az_questions` (
`id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`id_az_quiz` MEDIUMINT UNSIGNED NOT NULL,
`id_az_user` MEDIUMINT UNSIGNED NOT NULL,
`name` VARCHAR(200) NOT NULL,
`order_number` TINYINT UNSIGNED NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE = utf8_czech_ci;

CREATE TABLE `az_answers` (
`id` INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
`id_az_quiz` INTEGER UNSIGNED NOT NULL,
`id_az_question` INTEGER UNSIGNED NOT NULL,
`id_az_user` MEDIUMINT UNSIGNED NOT NULL,
`name` VARCHAR(200) NOT NULL,
`right_answer` ENUM('ano','ne') NOT NULL,
`order_number` TINYINT UNSIGNED NOT NULL
) ENGINE = MYISAM DEFAULT CHARSET=utf8 COLLATE = utf8_czech_ci;

ALTER TABLE az_quizs ADD INDEX id_az_user_i (id_az_user);

ALTER TABLE az_questions ADD INDEX id_az_quiz_i (id_az_quiz);
ALTER TABLE az_questions ADD INDEX id_az_user_i (id_az_user);

ALTER TABLE az_answers ADD INDEX id_az_question_i (id_az_question);
ALTER TABLE az_answers ADD INDEX id_az_user_i (id_az_user);
ALTER TABLE az_answers ADD INDEX id_az_quiz_i (id_az_quiz);

ALTER TABLE az_quizs ADD COLUMN have_options ENUM('ano','ne') DEFAULT 'ano';