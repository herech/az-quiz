<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<?php $uzivatelskeUdaje = Uzivatel::VratUzivatelskeUdaje(); ?>
			<div id="obsah">
					<h1>Nastavení účtu</h1>
					<div id="nastaveni-uctu-obsah-podobal">
						<h3>Fakta o vašem účtu</h3>
						<p>Datum založení účtu: <?php $nahrada = preg_replace("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) (.*)$/","\\3. \\2. \\1 v \\4",$uzivatelskeUdaje['date']); 
										echo preg_replace(array("/^0/","/\. 0/"),array("",". "),$nahrada); ?>
						</p>
						<p>Pořadí založení účtu: <?php echo $uzivatelskeUdaje['id']; ?></p>
						<hr />
						<h3>Uživatelské údaje</h3>
						<p>Přihlašovací jméno: <?php echo $uzivatelskeUdaje['login']; ?></p>
						<p>Email: <?php echo $uzivatelskeUdaje['email']; ?></p>
						<hr />
						<div style="margin-top: 20px;">
							<p><a href="#" id="nastaveni-uctu-zmen-email">Změnit email</a> (Jen v nezbytných případech)</p>
							<p><a href="#" id="nastaveni-uctu-zmen-heslo">Změnit heslo</a></p>
						</div>
						<div id="zmenit-heslo">
							<form action="zmenit-heslo.php" method="post" id="form-zmena-hesla">
								<label for="password-old">Staré heslo:</label>
								<input type="password" name="password-old" id="password-old1" required="true" autocomplete="off">
								<label for="password-new">Nové heslo:</label>
								<input type="password" name="password-new" id="password-new1" required="true" autocomplete="off">
								<label for="password-new-check">Nové heslo (potvrzení):</label>
								<input type="password" name="password-new-check" id="password-new-check1" required="true" autocomplete="off">
								<div id="hesla-stejne">Hesla jsou stejné</div><div id="hesla-nestejne">Hesla nejsou stejné!</div>
								<input type="submit" value="Změň heslo">
							</form>
						</div>
						<div id="zmenit-email">
							<form action="zmenit-email.php" method="post" id="form-zmena-emailu">
								<label for="password-current">Vaše současné heslo:</label>
								<input type="password" name="password-current" id="password-current" required="true" autocomplete="off">
								<label for="email-new">Nová emailová adresa:</label>
								<input type="email" name="email-new" id="email-new" required="true" autocomplete="off">
								<input type="submit" value="Změň email">
							</form>
						</div>
					</div>
					
			</div>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
		<script type="text/javascript">
		$(function() {
		
			$("#nastaveni-uctu-zmen-email").bind("click", function(kliknuti) {
				kliknuti.preventDefault();
				$("#zmenit-heslo").fadeOut("slow", function() {
					$("#zmenit-email").fadeIn("slow");
				});
			});
			$("#nastaveni-uctu-zmen-heslo").bind("click", function(kliknuti) {
				kliknuti.preventDefault();
				$("#zmenit-email").fadeOut("slow", function() {
					$("#zmenit-heslo").fadeIn("slow");
				});
			});
			$("#password-new-check").keyup(function() {
				prvniHeslo = $("#password-new").val();
				druheHeslo = $("#password-new-check").val();
				heslaSeRovnaji = (prvniHeslo == druheHeslo) ? true : false;
				if (heslaSeRovnaji) {
					$("#hesla-nestejne").css("display","none");
					$("#hesla-stejne").css("display","block");
					heslaSeRovnaji = true;
				}
				else {
					$("#hesla-stejne").css("display","none");
					$("#hesla-nestejne").css("display","block");
					heslaSeRovnaji = false;
					
				}
			});
			
			$("#form-zmena-hesla").submit(function(funkce) {
				/*var password-old1 = $("#password-old1").val();
				var prvniHeslo1 = $("#password-new1").val();
				var druheHeslo1 = $("#password-new-check1").val();
				
				if ((prvniHeslo1 != druheHeslo1) || ((prvniHeslo1 == "") || (druheHeslo1 == "") || (password-old1 == ""))) {
					if (prvniHeslo1 != druheHeslo1) {
						ZobrazChybovouZpravu("Hesla jsou různé!");
					}
					if (((prvniHeslo1 == "") && (druheHeslo1 == "")) || (password-old1 == "")) {
						ZobrazChybovouZpravu("Nevyplnili jste pole pro hesla!");
					}
					funkce.preventDefault();
					return false;
				}
				else {*/
					return true;
				//}
			});
			
			$("#form-zmena-emailu").submit(function(funkce) {
				emailNew = $("#email-new").val();
				passwordCurrent = $("#password-current").val();
				if (!(JeRetezecVeFormatuMailu(emailNew)) || (passwordCurrent == "")) {
					if (!(JeRetezecVeFormatuMailu(emailNew))) {
						ZobrazChybovouZpravu("Email nemá správný formát");
					}
					if (passwordCurrent == "") {
						ZobrazChybovouZpravu("Nevyplnil jste pole pro heslo!");
					}
					funkce.preventDefault();
					return false;
				}
				else {
					return true;
				}
			});
			
			
			/*function JeRetezecVeFormatuMailu(retezec) { 
				regVyraz = new RegExp("^[^@]+@[^@]+\.[^@]{2,10}$"); 
				vysledekRegVyrazu = retezec.search(regVyraz);
				if (vysledekRegVyrazu == -1)
					return false;
				else 
					return true;
			}*/
			
			/*function ZobrazChybovouZpravu(chybovaZprava) {
				alert(chybovaZprava);
			}*/
			
		});
		</script>
	</body>
</html>
