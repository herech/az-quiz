$(function() {
			
			inputPrvniNahodneCislo = Math.floor((Math.random() * 10) + 1);
			inputDruheNahodneCislo = Math.floor(Math.random() * 10);
			inputHiddenVysledekSouctu = inputPrvniNahodneCislo + inputDruheNahodneCislo;
			
			$(".inputPrvniNahodneCislo").val(inputPrvniNahodneCislo);
			$(".inputDruheNahodneCislo").val(inputDruheNahodneCislo);
			$(".inputHiddenVysledekSouctu").val(inputHiddenVysledekSouctu);
		
			$("a#odkaz-zapomenute-heslo").bind("click", function(kliknuti) {
				kliknuti.preventDefault();
				$("div#obal-prihlaseni").find("h2:eq(0), form:eq(0)").fadeOut("slow",function(){
					$("div#obal-prihlaseni").find("h2:eq(1), form:eq(1)").fadeIn("slow");
				});
			});
			$("#password2").keyup(function() {
				prvniHeslo = $("#password1").val();
				druheHeslo = $("#password2").val();
				heslaSeRovnaji = (prvniHeslo == druheHeslo) ? true : false;
				if (heslaSeRovnaji) {
					$("#hesla-nestejne").css("display","none");
					$("#hesla-stejne").css("display","block");
					heslaSeRovnaji = true;
				}
				else {
					$("#hesla-stejne").css("display","none");
					$("#hesla-nestejne").css("display","block");
					heslaSeRovnaji = false;
					
				}
			});
			
			$("#registrace").submit(function() {
				email1 = $("#email1").val();
				password1 = $("#password1").val();
				password2 = $("#password2").val();
				
				inputVysledekSouctu = $('form[action="registrace.php"] .inputVysledekSouctu').val();
				if (!(JeRetezecVeFormatuMailu(email1)) || (heslaSeRovnaji == false) || (inputVysledekSouctu != inputHiddenVysledekSouctu) || ((password1 == "") || (password2 == ""))) {
					if (!(JeRetezecVeFormatuMailu(email1))) {
						ZobrazChybovouZpravu("Email nemá správný formát");
						$("#neplatny-email").css("display","block");
					}
					if (heslaSeRovnaji == false) {
						ZobrazChybovouZpravu("Hesla jsou různé!");
					}
					if (inputVysledekSouctu != inputHiddenVysledekSouctu) {
						ZobrazChybovouZpravu("Nesprávný kontrolní součet!");
					}
					if ((password1 == "") && (password2 == "")) {
						ZobrazChybovouZpravu("Nevyplnili jste pole pro hesla!");
					}
					return false;
				}
				else {
					return true;
				}
			});
			
			$("#prihlaseni").submit(function() {
				email2 = $("#email2").val();
				password3 = $("#password3").val();
				inputVysledekSouctu = $('form[action="prihlaseni.php"] .inputVysledekSouctu').val();
				if (!(JeRetezecVeFormatuMailu(email2)) || (inputVysledekSouctu != inputHiddenVysledekSouctu) || (password3 == "")) {
					if (!(JeRetezecVeFormatuMailu(email2))) {
						ZobrazChybovouZpravu("Email nemá správný formát");
					}
					if (inputVysledekSouctu != inputHiddenVysledekSouctu) {
						ZobrazChybovouZpravu("Nesprávný kontrolní součet!");
					}
					if (password3 == "") {
						ZobrazChybovouZpravu("Nevyplnil jste pole pro heslo!");
					}
					return false;
				}
				else {
					return true;
				}
			});
			
			$("#zapomenute-heslo").submit(function() {
				email3 = $("#email3").val();
				password4 = $("#password4").val();
				inputVysledekSouctu = $('form[action="zapomenute-heslo.php"] .inputVysledekSouctu').val();
				if (!(JeRetezecVeFormatuMailu(email3)) || (inputVysledekSouctu != inputHiddenVysledekSouctu) || (password4 == "")) {
					if (!(JeRetezecVeFormatuMailu(email3))) {
						ZobrazChybovouZpravu("Email nemá správný formát");
					}
					if (inputVysledekSouctu != inputHiddenVysledekSouctu) {
						ZobrazChybovouZpravu("Nesprávný kontrolní součet!");
					}
					if (password4 == "") {
						ZobrazChybovouZpravu("Nevyplnil jste pole pro heslo!");
					}
					return false;
				}
				else {
					return true;
				}
			});
			
			function JeRetezecVeFormatuMailu(retezec) { 
				regVyraz = new RegExp("^[^@]+@[^@]+\.[^@]{2,10}$"); 
				vysledekRegVyrazu = retezec.search(regVyraz);
				if (vysledekRegVyrazu == -1)
					return false;
				else 
					return true;
			}
			
			function ZobrazChybovouZpravu(chybovaZprava) {
				alert(chybovaZprava);
			}
			
		});