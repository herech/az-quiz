<?php 

class ValidacniTrida {

	protected static $inputVysledekSouctu;
	protected static $inputHiddenVysledekSouctu;
	
	public static function NaplnTridniVlastnosti() {		
			self::$inputVysledekSouctu = $_POST['inputVysledekSouctu'];
			self::$inputHiddenVysledekSouctu = $_POST['inputHiddenVysledekSouctu'];		
	}

	public static function OverKontrolniSoucet() {
		
		self::NaplnTridniVlastnosti();
		
		if ((empty(self::$inputHiddenVysledekSouctu)) || (self::$inputVysledekSouctu != self::$inputHiddenVysledekSouctu))
			return false;
		else 
			return true;
	}
	
	public static function OverEmailUzivatele($email) {
		
		if (preg_match("~^[^@]+@[^@]+\.[^@]{2,10}$~", $email))
			return true;
		else 
			return false;
	
	}
}

?>