<?php 
class Kviz {

	protected static $nazevKvizu;
	protected static $otazky;
	protected static $odpovedi;
	protected static $public;
	protected static $idKvizu;
	protected static $cisloOtazky;
	protected static $nazevPredchoziStranky;
	protected static $typKvizu;

	protected static function NaplnTridniAtributy() {
		if (isset($_POST['otazky'])) {
			self::$otazky = $_POST['otazky'];
			self::$odpovedi = $_POST['odpovedi'];
			self::$nazevKvizu = $_POST['nazev-kvizu'];
			
			if(isset($_POST['public']) && $_POST['public'] == 'ano') {
				self::$public = "ano";
			}
			else {
				self::$public = "ne";
			}
			if (isset($_POST['idKvizu'])) {
				self::$idKvizu = $_POST['idKvizu'];
			}
			if (isset($_POST['urlPredchoziStranky'])) {
				self::$nazevPredchoziStranky = $_POST['nazevPredchoziStranky'];
			}
			if (isset($_POST['typKvizu'])) {
				self::$typKvizu = $_POST['typKvizu'];
			}
			
			return true;
		}
		else {
			//throw new Exception("Kon");
			return false;
		}
	}
	
	public static function VytvorNovyKviz() {
		$databaze = new Databaze();
		$databaze->VytvorNovyKviz(self::$nazevKvizu, self::$public, self::$otazky, self::$odpovedi, self::$typKvizu);
	}

	public static function OverUdajeZadaneUzivatelem() {
		/*if (isset($_POST)) {
			foreach ($_POST as $key => $value) {
				if (is_array($value)) {
					foreach ($value as $key1 => $value1) {
						echo htmlspecialchars($value1);
					}
				}
				else {
					echo htmlspecialchars($value);
				}
			}
		}*/
		
		if (!(self::NaplnTridniAtributy())) {
			//throw new Exception("Konec");
			return false;
		}
		if ((Sessions::VratIdUzivatele() == 0) || (Sessions::VratIdUzivatele() == "")) {
			setcookie("nazevKvizu", self::$nazevKvizu, time()+30);  
			setcookie("public", self::$public, time()+30);  
			setcookie("otazky", implode("||", self::$otazky), time()+30);  
			setcookie("odpovedi", implode("||",self::$odpovedi), time()+30);  
			if (self::$nazevPredchoziStranky == "upravit-kviz.php") {
				header("Location: ". Server::VratDomenu() ."/upravit-kviz.php?idKvizu=". self::$idKvizu ."");
			}
			else if (self::$nazevPredchoziStranky == "vytvoreni-noveho-kvizu.php") {
				header("Location: ". Server::VratDomenu() ."/vytvoreni-noveho-kvizu.php");
			}
			else return false;
		}
		if (self::$nazevKvizu == "") {
				//throw new Exception("Konec1");
				return false;
			}
		
		$pocetOtazek = count(self::$otazky);
		for ($i = 0; $i < $pocetOtazek; $i++) {
			if (self::$otazky[$i] == "") {
				//throw new Exception("Konec2");
				return false;
			}
		}
		$pocetOdpovedi = count(self::$odpovedi);
		for ($i = 0; $i < $pocetOdpovedi; $i++) {
			if (self::$odpovedi[$i] == "") {
				//throw new Exception("Konec3");
				return false;
			}
			else 
				return true;
		}
			
	}
	
	public static function ExistujiVytvoreneKvizy() {
		$databaze = new Databaze();
		if ($databaze->ExistujiVytvoreneKvizy())
			return true;
		else 
			return false;
	}
	
	public static function VratSeznamKvizu() {
		$databaze = new Databaze();
		$seznamKvizu = $databaze->VratSeznamKvizu();	
		return $seznamKvizu;
	}
	
	public static function SmazKviz(){
	
		if (!isset($_GET['idKvizu']))
			return false;
		else {
			self::$idKvizu = $_GET['idKvizu'];
			$databaze = new Databaze();
			if ($databaze->SmazKviz(self::$idKvizu))
				return true;
			else
				return false;
		}
		
	}
	public static function UpravKviz() {
	
		if (!isset($_POST['idKvizu']))
			return false;
		else {
			self::$idKvizu = $_POST['idKvizu'];
			self::NaplnTridniAtributy(); //naplníme zbývající atributy, které potřebuje funkce ve třídě databaze
			$databaze = new Databaze();
			if ($databaze->UpravKviz(self::$idKvizu, self::$nazevKvizu, self::$public, self::$otazky, self::$odpovedi))
				return true;
			else
				return false;
		}
		
	}
	
	public static function ExistujeKviz($idKvizu) {
		$databaze = new Databaze();
		if ($databaze->ExistujeKviz($idKvizu))
			return true;
		else 
			return false;
	
	} 
	
	public static function VratVlastnostiKvizu($idKvizu){
		$databaze = new Databaze();
		$vlastnostiKvizu = $databaze->VratVlastnostiKvizu($idKvizu);
		return $vlastnostiKvizu;
		
	} 
	
	public static function VratOtazkyKvizu($idKvizu) {
		$databaze = new Databaze();
		$otazkyKvizu = $databaze->VratOtazkyKvizu($idKvizu);
		return $otazkyKvizu;
	}
	
	public static function VratOdpovediKvizu($idKvizu) {
		$databaze = new Databaze();
		$odpovediKvizu = $databaze->VratOdpovediKvizu($idKvizu);
		return $odpovediKvizu;
	}
	
	public static function VratDataDoOknaKvizu() {
		self::$idKvizu = $_POST['idKvizu'];
		self::$cisloOtazky = $_POST['cisloOtazky'];
		$databaze = new Databaze();
		$vracenaData = $databaze->VratDataDoOknaKvizu(self::$idKvizu, self::$cisloOtazky);
		/*$vracenaData["otazka"] = "Jak se daří";
			$vracenaData["odpoved1"] = "dobře";
			$vracenaData["odpoved2"] = "špatně";
			$vracenaData["odpoved3"] = "velmi špatně";*/
		return $vracenaData;
		//echo json_encode($vracenaData);
	}
	
	public static function JeVerejnyProSoucasnehoUzivatele($idKvizu) {
		$databaze = new Databaze();
		if ($databaze->JeKvizVerejnyProSoucasnehoUzivatele($idKvizu))
			return true;
		else return false;
	} 
	
}
?>