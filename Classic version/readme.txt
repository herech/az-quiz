Aplikaci az kv�z vytvo�il Jan Herec[herech@seznam.cz] v r�mci SO� 2012, obor: Informatika.
Rozhodli jste se pou��vat aplikaci online az kv�z a j� v�m d�kuji za podporu a d�v�ru. 
Ne� budete pokra�ovat ve �ten� tohoto souboru, doporu�uji nejprve p�e��st licen�n� podm�nky, kter� jsou sou��st� t�to slo�ky.

Pot�ebn� provozn� podm�nky aplikace:

1) Vlastn� webov� prostor (hosting) a to na serveru:
	- na kter�m je nainstalov�no minim�ln� PHP ve verzi 5
	- na kter�m b�� datab�zov� server Mysql + vy osobn� m�te na tomto serveru vytvo�enou datab�zi (nejl�pe pojmenovanou jako azkviz)
	- na kter�m b�� n�stroj pro spr�vu datab�ze (nap�. PhpMyAdmin)
	- kter� podporuje ajaxov� po�adavky
	- kter� m� standardn� nastaven� PHP direktiv (pokud dan� server pou��v� n�jak� nestandartn� � nebo sp� chybn� nastaven� php direktiv, pravd�podobn� se to dozv�te bu� po, nebo p�i instalaci, v tom p��pad� je dobr� zauva�ovat o zm�n� hostingu)

Proces instalace:
1) Obsah souboru query.sql, kter� je sou��st� t�to slo�ky je nutn� pomoc� online n�stroje pro spr�vu datab�ze (ka�d� norm�ln� hosting n�jak� m� � v�t�inou je to PhpMyAdmin) zadat - prov�st jako sql p��kaz ve va�� vytvo�en� datab�zi (pokud m�te mo�nost vytvo�te si datab�zi pojmenovanou jako azkviz).
2) V souboru class.Konstanty.php je nutn� vyplnit p�ipojovac� �daje k pou�it� datab�zi.
3) Obsah t�to slo�ky (kter� je pravd�podobn� pojmenovan� jako az kv�z), nikoliv slo�ku samotnou je pot�eba nahr�t pomoc� ftp na v� webov� prostor/server.

A to je v�e. Nyn� se m��ete registrovat, p�ihl�sit a u��vat tuto povedenou aplikaci. Samoz�ejm� v r�mci licen�n�ch podm�nek.

