<?php
ini_set("session.gc_maxlifetime", 60*60);
session_start(); 
//session_regenerate_id();
header("Cache-Control: no-cache, must-revalidate"); 
if (preg_match("/vratDataDoOknaKvizu/",$_SERVER['SCRIPT_NAME'])) {
	header('Content-type: text/plain;charset=utf-8');
}
else {
	header('Content-type: text/html;charset=utf-8');
}
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

if (isset($_GET)) {
	foreach ($_GET as $key => $value) {
		$_GET[$key] = htmlspecialchars($value);
	}
}
if (isset($_POST)) {
	foreach ($_POST as $key => $value) {
		if (is_array($value)) {
			foreach ($value as $key1 => $value1) {
				$_POST[$key][$key1] = htmlspecialchars($value1);
			}
		}
		else {
			$_POST[$key] = htmlspecialchars($value);
		}
	}
}
?>