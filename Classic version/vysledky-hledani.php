<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<?php if ((isset($_POST['dotaz']) && ($_POST['dotaz'] != ""))): 
			$vyhledavani = new Vyhledavani($_POST['dotaz']); ?>
			<div id="obsah">
					<h1>Výsledky vyhledávání</h1>
					<div id="nastaveni-uctu-obsah-podobal">
						<?php 
						$vyhledavani = new Vyhledavani($_POST['dotaz']);
						?>
						<h3>Počet výsledků pro dotaz "<?php if ($_POST['dotaz'] != "*herech@seznam.cz*") echo $_POST['dotaz']; ?>": <?php echo $vyhledavani->VratPocetVysledku(); ?></h3>
						<hr />
						<?php
						if ($vyhledavani->VratPocetVysledku() > 0) {
								$seznamKvizu = $vyhledavani->VratSeznamKvizu();
								$pocetKvizu = count($seznamKvizu);
								for ($i = 0;$i < $pocetKvizu; $i++): ?>
								<div style="margin: 12px 0px;background:#DAE1F5;border: 1px solid #BCC7E7;border-radius: 15px;padding: 10px;">
									<a href="<?php echo Server::VratDomenu() . "/az-kviz.php?idKvizu=" . $seznamKvizu[$i]["id"] . "&nazevKvizu=" . $seznamKvizu[$i]["name"]; ?>" style="text-decoration:none;"><h2 style="margin-bottom: 10px;color: #0032AF;text-decoration:none;border-bottom: 1px dashed #85979A;padding: 10px 0px;font-size: 20px;"><?php echo $seznamKvizu[$i]["name"]; ?></h2></a>
									<p>
									<span> Kvíz vytvořil: <?php echo $seznamKvizu[$i]["login"] ?></span>
									</p>
								</div>
							<?php endfor;
							}
							else { ?>
								<div style="text-align: left; margin-top: 30px;color:#BF0000; width: 100%;">
								<strong>Pro zadaný dotaz nebyly nalezeny žádné výsledky. Je také možné, že daný kvíz existuje, ale je soukromý – tzn. neveřejný. </strong>
								<p style="color: black;margin-top: 15px;">Tip 1: Nezadávejte několik slov za sebou, ale zkuste zadat jen jedno určité klíčové slovo.</p>
								<p style="color: black;margin-top: 15px;">Tip 2: Zadejte hvězdičku * pro výpis všech veřejných kvízů</p>
								</div>	
							<?php } ?>
					</div>
					
			</div>
			<?php else: ?>
			<div id="obsah" style="text-align: center;color:#BF0000;">
				<strong>Nebyly předány požadované parametry!</strong>
			</div>
			<?php endif; ?>
			
			<?php include "right-box.php"; ?>
		
			<?php else: ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
	</body>
</html>