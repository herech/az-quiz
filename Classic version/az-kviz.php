<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
require_once "require-only-for-html-pages.php";
?>

<!DOCTYPE html>
<html lang="cs-cz">
	<head>
		<?php include "head.php"; ?>
	</head>
	<body>
		<?php include "header.php";	?>
			
			<?php if (Uzivatel::JePrihlasen()): ?>
			<?php if ((isset($_GET['idKvizu']) && isset($_GET['nazevKvizu'])) && (($_GET['idKvizu'] != "") && ($_GET['nazevKvizu'] != ""))): ?>
			<?php if (Kviz::JeVerejnyProSoucasnehoUzivatele($_GET['idKvizu'])): ?>
			<input type="hidden" id="skrytyInputSIdKvizu" value="<?php echo $_GET['idKvizu']; ?>">
			<div id="obsah">
					<h1 style="margin-bottom:-20px;"><?php echo urldecode($_GET['nazevKvizu']); ?></h1>
					<div class="kviz-hraci">
					<img src="../img/prvni-hrac.png" /> <span> První hráč </span>
					<img src="../img/druhy-hrac.png"/> <span>Druhý hráč</span>
					</div>
					<div class="azkviz">
							<div class="obal-policka" data-cislo="1" data-barva="modra"><a class="policko" id="1" href="#">1</a></div>
							<div class="obal-policka" data-cislo="2" data-barva="modra"><a class="policko" id="2" href="#">2</a></div>
							<div class="obal-policka" data-cislo="3" data-barva="modra"><a class="policko" id="3" href="#">3</a></div>
							<div class="obal-policka" data-cislo="4" data-barva="modra"><a class="policko" id="4" href="#">4</a></div><!--hhhhh-->
							<div class="obal-policka" data-cislo="5" data-barva="modra"><a class="policko" id="5" href="#">5</a></div>
							<div class="obal-policka" data-cislo="6" data-barva="modra"><a class="policko" id="6" href="#">6</a></div>
							<div class="obal-policka" data-cislo="7" data-barva="modra"><a class="policko" id="7" href="#">7</a></div>
							<div class="obal-policka" data-cislo="8" data-barva="modra"><a class="policko" id="8" href="#">8</a></div>
							<div class="obal-policka" data-cislo="9" data-barva="modra"><a class="policko" id="9" href="#">9</a></div>
							<div class="obal-policka" data-cislo="10" data-barva="modra"><a class="policko" id="10" href="#">10</a></div>
							<div class="obal-policka" data-cislo="11" data-barva="modra"><a class="policko" id="11" href="#">11</a></div>
							<div class="obal-policka" data-cislo="12" data-barva="modra"><a class="policko" id="12" href="#">12</a></div>
							<div class="obal-policka" data-cislo="13" data-barva="modra"><a class="policko" id="13" href="#">13</a></div>
							<div class="obal-policka" data-cislo="14" data-barva="modra"><a class="policko" id="14" href="#">14</a></div>
							<div class="obal-policka" data-cislo="15" data-barva="modra"><a class="policko" id="15" href="#">15</a></div>
							<div class="obal-policka" data-cislo="16" data-barva="modra"><a class="policko" id="16" href="#">16</a></div>
							<div class="obal-policka" data-cislo="17" data-barva="modra"><a class="policko" id="17" href="#">17</a></div>
							<div class="obal-policka" data-cislo="18" data-barva="modra"><a class="policko" id="18" href="#">18</a></div>
							<div class="obal-policka" data-cislo="19" data-barva="modra"><a class="policko" id="19" href="#">19</a></div>
							<div class="obal-policka" data-cislo="20" data-barva="modra"><a class="policko" id="20" href="#">20</a></div>
							<div class="obal-policka" data-cislo="21" data-barva="modra"><a class="policko" id="21" href="#">21</a></div>
							<div class="obal-policka" data-cislo="22" data-barva="modra"><a class="policko" id="22" href="#">22</a></div>
							<div class="obal-policka" data-cislo="23" data-barva="modra"><a class="policko" id="23" href="#">23</a></div>
							<div class="obal-policka" data-cislo="24" data-barva="modra"><a class="policko" id="24" href="#">24</a></div>
							<div class="obal-policka" data-cislo="25" data-barva="modra"><a class="policko" id="25" href="#">25</a></div>
							<div class="obal-policka" data-cislo="26" data-barva="modra"><a class="policko" id="26" href="#">26</a></div>
							<div class="obal-policka" data-cislo="27" data-barva="modra"><a class="policko" id="27" href="#">27</a></div>
							<div class="obal-policka" data-cislo="28" data-barva="modra"><a class="policko" id="28" href="#">28</a></div>
						</div>
					<footer>
					</footer>
			</div>
			<?php else: ?>
			<div id="obsah" style="text-align: center;color:#BF0000">
				<strong>Tento kvíz je soukromý, nemáte oprávnění jej spouštět!</strong>
			</div>
			<?php endif; ?>
			<?php else: ?>
			<div id="obsah" style="text-align: center;color:#BF0000">
				<strong>Pro načtení kvízu nebyly předány potřebné parametry!</strong>
			</div>
			<?php endif; ?>
			<?php include "right-box.php"; ?>
			
			<?php else: ?>
			<?php if ($_GET['odhlasen'] == "ano") echo "Jsi odhlašen!"; ?>
			
			<?php include "Inc.prihlaseni.php"; ?>
		
			<?php endif; ?>
		</div>
		<?php include "footer.php"; ?>
	</body>
</html>