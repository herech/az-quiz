<?php 
require_once "php-hlavicky.php";
require_once "SouborTrid.php";
/*HLAVIČKA KVŮLI ENDOŘE.CZ*/

$vracenaData = Uzivatel::VratDataDoOknaKvizu(); 
$spravnaOdpoved = "";
/*$vracenaData["otazka"] = "Jak se daří";
			$vracenaData["odpoved1"] = "dobře";
			$vracenaData["odpoved2"] = "špatně";
			$vracenaData["odpoved3"] = "velmi špatně";*/
			
if (($_POST["kolikatyKlik"] % 2) == 0) {
	$otazkaProHrace = "druhého hráče";
}
else {
	$otazkaProHrace = "prvního hráče";
}
			
if (Databaze::ObsahujeKvizMoznosti($_POST['idKvizu'])) { 
	$prvniMoznost = "<li><a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ano\">" . $vracenaData["odpoved1"] . "</a></li>";
	$druhaMoznost = "<li><a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ne\">" . $vracenaData["odpoved2"] . "</a></li>";
	$tretiMoznost = "<li><a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ne\">" . $vracenaData["odpoved3"] . "</a></li>";

	$poleOdpovedi = array($prvniMoznost,$druhaMoznost,$tretiMoznost);
	shuffle($poleOdpovedi);

	$OdpovedZAjaxu = "<h3>". $vracenaData["otazka"] ."</h3></div><div id=\"odpovedi-z-ajaxu\"><ol>"
									. $poleOdpovedi[0]
									. $poleOdpovedi[1]
									. $poleOdpovedi[2]
								."</ol></div>";
	$jeTentoKvizSMoznostma = "ano";
}
else {
	$zobrazSpravnouOdpoved = "<a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ano\" id=\"click-kviz-bez-moznosti\" style=\"background: #2C7E7E;color:white;text-decoration:none; padding: 7px; border-radius: 7px;font-weight:bold;\">Zobraz správnou odpověď</a>";
	$prvniMoznost = "<li><a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ano\">" . "Hráč odpověděl správně" . "</a></li>";
	$druhaMoznost = "<li><a class=\"moznosti\" href=\"#\" data-spravnaOdpoved=\"ne\">" . "Hráč odpověděl nesprávně" . "</a></li>";

	$OdpovedZAjaxu = "<h3>". $vracenaData["otazka"] ."</h3></div><div id=\"odpovedi-z-ajaxu\"><ul>"
									. $zobrazSpravnouOdpoved
									."</ul></div>";
	$jeTentoKvizSMoznostma = "ne";
	$spravnaOdpoved = $vracenaData["odpoved1"];
}

								
$otazkaPlusOdpovedi = "<div id=\"pocetZbyvajicichSekund-obal\"><span id=\"pocetZbyvajicichSekund\">Zbývající čas: </span><span id=\"pocetZbyvajicichSekund1\"></span></div>"
								. "<div style=\"background: #DEE7E8;border-bottom: 1px solid #BBD2D6;margin-bottom: 30px;\">"
								. "<div id=\"otazkaProHrace\">(Otázka pro " . $otazkaProHrace . ")</div>"
								. $OdpovedZAjaxu . "<div id=\"zobraz-vysledek\" style=\"clear:both;\"></div><div  style=\"display:none;\" id=\"jeTentoKvizSMoznostma\">". $jeTentoKvizSMoznostma ."</div><div style=\"display:none;\" id=\"spravnaOdpoved\">".$spravnaOdpoved."</div>";

echo $otazkaPlusOdpovedi;
/*$data = array(
	'otazka' => "Kolik je hodin?",$_POST['cisloOtazky']
	'odpoved1'   => '1',
	'odpoved2'   => '2',
	'odpoved3'   => '3'
);

// zakodujeme pole do JSON formatu a vypiseme
echo json_encode($data);*/

?>
